export class User {
    constructor(
        public email: string,
        public userId: number,
        public firstName: string,
        public lastName: string,
        public fullName: string,
        public pseudo: string,
    ) {
    }
}
