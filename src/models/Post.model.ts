import Code from './Code.model';
import { User } from './User.model';

export default class Post {
    readonly id?: number;
    readonly title?: string;
    readonly content?: string;
    readonly codes?: Code[];
    readonly authorId?: number;
    readonly hasUserUpVoted?: boolean;
    readonly hasUserDownVoted?: boolean;
    readonly score?: number;
    readonly author?: User;
    readonly countComments?: number;
    readonly createdAt?: string;
    readonly updatedAt?: string;
}