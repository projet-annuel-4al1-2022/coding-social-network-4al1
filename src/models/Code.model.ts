export default class Code {
    title: string;
    code: string;
    language: string;
}