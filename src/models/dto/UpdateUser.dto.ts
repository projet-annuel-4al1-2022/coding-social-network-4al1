export interface UpdateUserDto {
    readonly email: string,
    readonly firstName: string,
    readonly lastName: string,
    readonly pseudo: string,
    readonly password: string,
}