import Code from '../Code.model';

export class CreatePostDTO {
    readonly title: string;
    readonly content: string;
    readonly codes: Code[];
    readonly authorId: number;
}