import React from 'react';
import './Authentication.css';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { goToLogin } from '../../services/routing.service';
import { ColorService } from '../../services/color.service';

function Copyright(props: any) {
    return (
        <Typography
            variant='body2'
            color='text.secondary'
            align='center'
            {...props}
        >
            {'Copyright © '}
            <Link color='inherit' href='https://mui.com/'>
                Coding Social Network
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export class SignUp extends React.Component {
    constructor(props: any) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
        };
    }

    private static removeSignUpErrors() {
        const signUpError = document.getElementById('sign-up-error');
        signUpError.innerHTML = '';
    }

    private static addSignUpErrors() {
        const signUpError = document.getElementById('sign-up-error');
        signUpError.innerHTML = 'Champs incorrects';
    }

    componentDidMount() {
        const firstName = document.getElementById('firstName');
        const lastName = document.getElementById('lastName');
        const pseudo = document.getElementById('pseudo');
        const email = document.getElementById('email');
        const password = document.getElementById('password');
        firstName.addEventListener('input', () => SignUp.removeSignUpErrors());
        lastName.addEventListener('input', () => SignUp.removeSignUpErrors());
        pseudo.addEventListener('input', () => SignUp.removeSignUpErrors());
        email.addEventListener('input', () => SignUp.removeSignUpErrors());
        password.addEventListener('input', () => SignUp.removeSignUpErrors());
    }

    handleSubmit = (event: any) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        this.handleSignUp(
            data.get('firstName').toString(),
            data.get('lastName').toString(),
            data.get('pseudo').toString(),
            data.get('email').toString(),
            data.get('password').toString(),
        );

    };

    render() {
        return (
            <Container component='main' maxWidth='xs'>
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: ColorService.APP_SECONDARY_HOVER_COLOR }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component='h1' variant='h5'>
                        Inscription
                    </Typography>
                    <Box
                        component='form'
                        noValidate
                        onSubmit={this.handleSubmit}
                        sx={{ mt: 3 }}
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete='given-name'
                                    name='firstName'
                                    required
                                    fullWidth
                                    id='firstName'
                                    label='Prénom'
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id='lastName'
                                    label='Nom'
                                    name='lastName'
                                    autoComplete='family-name'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id='pseudo'
                                    label='Pseudo'
                                    name='pseudo'
                                    autoComplete='pseudo'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id='email'
                                    label='Email'
                                    name='email'
                                    autoComplete='email'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name='password'
                                    label='Mot de passe'
                                    type='password'
                                    id='password'
                                    autoComplete='new-password'
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <div
                                    id='sign-up-error'
                                    className='FormError'
                                />
                            </Grid>
                            {/*<Grid item xs={12}>*/}
                            {/*    <FormControlLabel*/}
                            {/*        control={*/}
                            {/*            <Checkbox*/}
                            {/*                value='allowExtraEmails'*/}
                            {/*                color='primary'*/}
                            {/*            />*/}
                            {/*        }*/}
                            {/*        label="Je souhaite recevoir par courrier électronique de l'inspiration, des promotions commerciales et des mises à jour."*/}
                            {/*    />*/}
                            {/*</Grid>*/}
                        </Grid>
                        <Button
                            className='CapitalizedButtonWithoutMargin'
                            type='submit'
                            fullWidth
                            variant='contained'
                            sx={{ mt: 3, mb: 2 }}
                        >
                            S'inscrire
                        </Button>
                        <Grid container justifyContent='flex-end'>
                            <Grid item>
                                <Link
                                    href='#'
                                    variant='body2'
                                    onClick={goToLogin}
                                >
                                    Vous avez déjà un compte ?
                                    Connectez-vous
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
                <Copyright sx={{ mt: 5 }} />
            </Container>
        );
    }

    private handleSignUp(
        firstName: string,
        lastName: string,
        pseudo: string,
        email: string,
        password: string,
    ) {
        const signUpUrl = process.env.REACT_APP_API_URL + '/auth/signup'; // 'http://localhost:3000/api/auth/signup';

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                firstName,
                lastName,
                pseudo,
                email,
                password,
            }),
        };

        fetch(signUpUrl, requestOptions)
            .then((res) => {
                if (200 < res['status'] && res['status'] > 299) {
                    this.setState({
                        isLoaded: true,
                        error: true,
                    });
                    throw new Error('error when sign up');
                }
                return res.json();
            })
            .then(
                () => {
                    this.setState({
                        isLoaded: true,
                        error: false,
                    });
                    goToLogin();
                },
                (err) => {
                    this.setState({
                        isLoaded: true,
                        error: true,
                    });
                    console.log(err);
                    SignUp.addSignUpErrors();
                },
            )
            .catch((err) => {
                this.setState({
                    isLoaded: true,
                    error: true,
                });
                console.log(err);
                SignUp.addSignUpErrors();
            });
    }
}
