import { useState } from 'react';
import { User } from '../../models/User.model';
import UserService from '../../services/user.service';

export default function useToken() {
    const getToken = () => {
        return UserService.getAccessToken();
    };

    const [token, setToken] = useState(getToken());

    const saveToken = (userAccessToken: string, user: User) => {
        console.log('Logging user: ' + user);
        UserService.setCurrentUser(user);
        UserService.setAccessToken(userAccessToken);
        setToken(userAccessToken);
    };

    return {
        setToken: saveToken,
        accessToken: token,
    };
}
