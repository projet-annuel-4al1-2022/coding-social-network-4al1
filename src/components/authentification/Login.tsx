import React, { useState } from 'react';
import './Authentication.css';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { goToDashboard, goToSignUp } from '../../services/routing.service';
import { User } from '../../models/User.model';
import { ColorService } from '../../services/color.service';

function Copyright(props: any) {
    return (
        <Typography
            variant='body2'
            color='text.secondary'
            align='center'
            {...props}
        >
            {'Copyright © '}
            <Link color='inherit' href='https://mui.com/'>
                Coding Social Network
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export function Login({ setToken }) {
    const [accessToken, setAccessToken] = useState(null);
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);

    const removeLoginErrors = () => {
        const loginError = document.getElementById('login-error');
        loginError.innerHTML = '';
    };

    const addLoginErrors = () => {
        const loginError = document.getElementById('login-error');
        loginError.innerHTML = 'Identifiants incorrects';
    };

    const handleSubmit = (event: any) => {
        event.preventDefault();
        handleLogin(email, password);
    };

    const handleLogin = (email: string, password: string) => {
        const loginApiUrl = process.env.REACT_APP_API_URL + '/auth/login'; // 'http://localhost:3000/api/auth/login';

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email,
                password,
            }),
        };

        fetch(loginApiUrl, requestOptions)
            .then((res) => {
                if (200 < res['status'] && res['status'] > 299) {
                    throw new Error('Failed login');
                }
                return res.json();
            })
            .then(
                (result) => {
                    const user: User = {
                        ...result.user,
                        userId: result.user.id,
                    };
                    setAccessToken(result.access_token);
                    setToken(result.access_token, user);
                    goToDashboard();
                },
                (err) => {
                    console.log(err);
                    addLoginErrors();
                },
            )
            .catch((err) => {
                console.log(err);
                addLoginErrors();
            });
    };

    return (
        <Container component='main' maxWidth='xs'>
            <CssBaseline />
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Avatar sx={{ m: 1, bgcolor: ColorService.APP_SECONDARY_HOVER_COLOR }}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component='h1' variant='h5'>
                    Connectez-vous
                </Typography>
                <Box
                    component='form'
                    onSubmit={handleSubmit}
                    noValidate
                    sx={{ mt: 1 }}
                >
                    <TextField
                        margin='normal'
                        required
                        fullWidth
                        id='email'
                        label='Email'
                        name='email'
                        autoComplete='email'
                        autoFocus
                        onChange={(e) =>
                            setEmail(e.target.value.toString())
                        }
                        onInput={removeLoginErrors}
                    />

                    <TextField
                        margin='normal'
                        required
                        fullWidth
                        name='password'
                        label='Mot de passe'
                        type='password'
                        id='password'
                        autoComplete='current-password'
                        onChange={(e) => setPassword(e.target.value)}
                        onInput={removeLoginErrors}
                    />
                    {/*<FormControlLabel*/}
                    {/*    control={*/}
                    {/*        <Checkbox value='remember' color='primary' />*/}
                    {/*    }*/}
                    {/*    label='Se souvenir de moi'*/}
                    {/*/>*/}
                    <div id='login-error' className='FormError' />
                    <Button
                        className='CapitalizedButtonWithoutMargin'
                        type='submit'
                        fullWidth
                        variant='contained'
                        sx={{ mb: 1 }}
                    >
                        Se connecter
                    </Button>
                    {/*<Button className={'CapitalizedButton'} variant='contained' onClick={handleClickOpen}*/}
                    {/*        startIcon={<AddIcon />} style={{ marginRight: 10 }}>*/}
                    {/*    Créer un nouveau post*/}
                    {/*</Button>*/}
                    <Grid container>
                        {/*<Grid item>*/}
                        {/*    <Link href='#' variant='body2'>*/}
                        {/*        Mot de passe oublié ?*/}
                        {/*    </Link>*/}
                        {/*</Grid>*/}
                        <Grid item>
                            <Link
                                href='#'
                                variant='body2'
                                onClick={goToSignUp}
                            >
                                {
                                    'Vous n\'avez pas de compte ? Inscrivez-vous !'
                                }
                            </Link>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
            <Copyright sx={{ mt: 8, mb: 4 }} />
        </Container>
    );
}
