import * as React from 'react';
import { useEffect, useState } from 'react';
import './Posts.css';
import { Button, ButtonBase, Card, CardActions, CardContent, CircularProgress, Grid, Typography } from '@mui/material';
import { User } from '../../../models/User.model';
import { Duration } from '../../../helpers/duration.helper';
import SyntaxHighlighter from 'react-syntax-highlighter';
import theme from 'react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark';
import { goToOtherUserProfile, goToPostDetails } from '../../../services/routing.service';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { PostService } from '../../dashboard/posts/Post.service';
import { openSandbox, voteOnPost } from '../../dashboard/posts/Posts';

export function UserPosts(user: User) {
    const [posts, setPosts] = useState(null);

    const getUserPosts = async (userId: number) => {
        const postsResponse = await PostService.getUserPosts(userId, user.userId);
        console.log(postsResponse);
        setPosts(postsResponse);
    };

    useEffect(() => {
        getUserPosts(user.userId);
    }, []);
    return (
        <Grid container>
            <Grid item>
                {!posts && <CircularProgress className='CircularLoader' />}
                {posts && posts.length === 0 && (
                    <Typography className='NoPostFound' variant='h6' style={{ marginTop: 10 }}>
                        Pas de post trouvé. Essayez de suivre quelqu'un !
                    </Typography>
                )}
                {posts && posts.length > 0 &&
                    <Typography variant='h2' style={{ marginTop: 40, marginBottom: 40, fontWeight: 'bold' }}
                        align={'center'}>
                        Mes posts
                    </Typography>
                }
                {posts && posts.length > 0 &&
                    posts.map((post) => {
                        return (
                            <Card variant='outlined' className='PostContainer' key={post.id}>
                                <Grid container flexDirection={'row'}>
                                    <Grid
                                        item
                                        xs={1}
                                        md={1}
                                        lg={1}
                                        flexDirection={'column'}
                                        alignItems={'center'}
                                        alignContent={'center'}
                                        alignSelf={'center'}
                                        justifyContent={'center'}
                                        justifyItems={'center'}
                                        paddingRight='10px'
                                    >
                                        <Button
                                            className='ButtonWithoutBackgroundAndBorders'
                                            onClick={() => {
                                                if (!post.hasUserUpVoted) {
                                                    voteOnPost(post, user, true);
                                                }
                                            }}
                                        >
                                            <KeyboardArrowUpIcon />
                                        </Button>
                                        <Typography
                                            id={'post' + post.id + 'score'}
                                            variant='h6'
                                            align={'center'}
                                            sx={{ minWidth: '64px', maxWidth: '64px' }}
                                            color={
                                                post.hasUserUpVoted
                                                    ? 'green'
                                                    : post.hasUserDownVoted
                                                        ? 'red'
                                                        : 'black'
                                            }
                                        >
                                            {post.score}
                                        </Typography>
                                        <Button
                                            className='ButtonWithoutBackgroundAndBorders'
                                            onClick={() => {
                                                if (!post.hasUserDownVoted) {
                                                    voteOnPost(post, user, false);
                                                }
                                            }}
                                        >
                                            <KeyboardArrowDownIcon />
                                        </Button>
                                    </Grid>
                                    <Grid item xs={11} md={11} lg={11}>
                                        <Typography
                                            sx={{
                                                fontSize: 14,
                                                color: 'text.secondary',
                                            }}
                                            align={'center'}
                                            style={{ width: '100%' }}
                                        >
                                            {Duration.timeSinceFrench(
                                                post.createdAt,
                                            )}
                                        </Typography>
                                        <ButtonBase
                                            className='ClickableCard'
                                            onClick={() => goToPostDetails(post.id)}
                                        >
                                            <CardContent className='PostContent'>
                                                <Typography
                                                    variant='h5'
                                                    color='text.secondary'
                                                    gutterBottom
                                                >
                                                    {post.title}
                                                </Typography>
                                                <Typography component='div' align='left'>
                                                    {post.content}
                                                </Typography>
                                                <br />
                                                {post.codes.length > 0 &&
                                                    post.codes.map((code) => {
                                                        return (
                                                            <>
                                                                <SyntaxHighlighter
                                                                    style={theme}
                                                                    className='HighlightedLanguage'
                                                                >
                                                                    {
                                                                        `/** Langage utilisé : ${code.language} **/`
                                                                    }
                                                                </SyntaxHighlighter>
                                                                <SyntaxHighlighter
                                                                    language={
                                                                        code.langage
                                                                    }
                                                                    style={theme}
                                                                    className='HighlightedCode'
                                                                >
                                                                    {code.code}
                                                                </SyntaxHighlighter>
                                                            </>
                                                        );
                                                    })}
                                            </CardContent>
                                        </ButtonBase>
                                        <CardActions>
                                            <Button
                                                size='large'
                                                onClick={() => openSandbox(post.codes[0].code, post.codes[0].language)}
                                                style={{ margin: 'auto' }}
                                            >
                                                Ouvrir une sandbox {'< />'}
                                            </Button>
                                        </CardActions>
                                        <CardContent className='PostContent'>
                                            <Typography component='div' align='left'>
                                                {post.countComments || 0} commentaires
                                            </Typography>
                                        </CardContent>
                                    </Grid>
                                </Grid>
                            </Card>
                        );
                    })}
            </Grid>
        </Grid>
    );
}

export const seeAuthor = (authorId: number) => {
    goToOtherUserProfile(authorId);
};
