import * as React from 'react';
import './PostsList.css';
import UserService from '../../../services/user.service';
import { UserPosts } from './UserPosts';

export default function UserPostsList() {
    const user = UserService.getCurrentUser();

    return (
        <UserPosts
            userId={user.userId}
            email={user.email}
            pseudo={user.pseudo}
            firstName={user.firstName}
            fullName={user.fullName}
            lastName={user.lastName}
        />
    );
}