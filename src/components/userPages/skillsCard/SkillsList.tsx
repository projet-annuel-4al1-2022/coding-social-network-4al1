import * as React from 'react';
import Card from '@mui/material/Card';
import { Grid } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';


export default function UserSkillsList() {
    return (
        <Card className='card-container'
            sx={{ width: 800, height: 300, background: 'white', marginLeft: 'auto', marginRight: 'auto', marginTop: '60px' }}
        >
            <Grid>
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB' alt="" width={100} height={50} />
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vue.js&logoColor=4FC08D' alt="" width={100} height={50} />
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white' alt="" width={100} height={50} />
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white' alt="" width={100} height={50} />
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white' alt="" width={100} height={50} />
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/Flutter-02569B?style=for-the-badge&logo=flutter&logoColor=white' alt="" width={100} height={50} />
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white' alt="" width={100} height={50} />
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white' alt="" width={100} height={50} />
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/Heroku-430098?style=for-the-badge&logo=heroku&logoColor=white' alt="" width={100} height={50} />
                <img style={{ margin: 10 }} src='https://img.shields.io/badge/Amazon_AWS-232F3E?style=for-the-badge&logo=amazon-aws&logoColor=white' alt="" width={100} height={50} />
                <AddCircleIcon style={{ margin: 20 }} />
            </Grid>

        </Card>
    );
}

