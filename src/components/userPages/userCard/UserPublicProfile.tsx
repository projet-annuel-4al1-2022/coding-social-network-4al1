import React, { useEffect, useState } from 'react';
import './UserPublicProfile.css';
import { User } from '../../../models/User.model';
import { useParams } from 'react-router-dom';
import {
    Button,
    ButtonBase,
    ButtonGroup,
    Card,
    CardActions,
    CardContent,
    CircularProgress,
    Grid,
    Typography,
} from '@mui/material';
import { Trends } from '../../dashboard/trends/Trends';
import { openSandbox, voteOnPost } from '../../dashboard/posts/Posts';
import { Followings } from '../../dashboard/followings/Followings';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { goToPostDetails, goToSnippets } from '../../../services/routing.service';
import { Duration } from '../../../helpers/duration.helper';
import theme from 'react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { UserAPIService } from '../UserAPIService';
import UserService from '../../../services/user.service';
import { PostService } from '../../dashboard/posts/Post.service';
import { seeAuthor } from '../postsCard/UserPosts';
import { Header } from '../../header/Header';

export function UserPublicProfile() {
    const user: User = UserService.getCurrentUser();

    const { userId: userProfileId } = useParams();
    const [userCurrentProfile, setUserCurrentProfile] = useState(null);
    const [userProfilePosts, setUserProfilePosts] = useState(null);

    const getUserProfile = async () => {
        if (userCurrentProfile) {
            return;
        }
        const userProfile = await UserAPIService.getUserProfile(parseInt(userProfileId), user.userId);
        setUserCurrentProfile(userProfile);
    };

    const getUserProfilePosts = async () => {
        const userProfilePosts = await PostService.getUserPosts(parseInt(userProfileId), user.userId);
        setUserProfilePosts(userProfilePosts);
    };

    useEffect(() => {
        getUserProfile();
        getUserProfilePosts();
    }, []);

    function followUser(userId, userIdToFollow) {
        userCurrentProfile.isUserFollowedByUserRequesting = true;
        UserAPIService.followUser(userId, userIdToFollow).then(() => {
            window.location.reload();
        });
    }

    function unfollowUser(userId, userIdToFollow) {
        userCurrentProfile.isUserFollowedByUserRequesting = false;
        UserAPIService.unfollowUser(userId, userIdToFollow).then(() => {
            window.location.reload();
        });
    }

    return (
        <div className='UserPublicProfile'>
            {(!userCurrentProfile || !user) && <CircularProgress className='CircularLoader' />}
            {userCurrentProfile && user && (
                <Grid container>

                    <Header
                        pageTitle={''}
                        leftButtonContent={'Mes snippets'}
                        leftButtonFunction={goToSnippets}
                    />

                    <Grid
                        item
                        xs={2}
                        md={2}
                        lg={2}
                        borderRight='1px solid lightgrey'
                        style={{ paddingTop: '30px' }}
                    >
                        <header className='TrendsSection'>
                            <h3>Tendances</h3>
                            <Trends
                                userId={user.userId}
                                email={user.email}
                                pseudo={user.pseudo}
                                firstName={user.firstName}
                                fullName={user.fullName}
                                lastName={user.lastName}
                            />
                        </header>
                    </Grid>

                    <Grid item xs={8} md={8} lg={8}>

                        {/* User profile with image, pseudo */}
                        {/* Button for DM + Follow / Unfollow */}
                        {/* bio */}
                        {/* followers, following */}
                        <Grid container style={{ background: '' }}>
                            <Grid container className={'UserFirstInfos'}>
                                <Grid item xs={4} md={4} lg={4} alignSelf={'center'}>
                                    <Grid container direction={'column'} alignItems={'end'}>
                                        <div className='image-user-other-profil'>
                                            <img
                                                src={require('../../../images/user-image.png')}
                                                alt='user-picture'
                                            />
                                        </div>
                                    </Grid>
                                </Grid>

                                <Grid item xs={4} md={4} lg={4} alignSelf={'center'}>
                                    <Grid container direction={'column'} alignItems={'start'}>
                                        <Grid item>
                                            <Typography variant='h5'>
                                                @{userCurrentProfile.pseudo}
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant='body1'>
                                                {userCurrentProfile.fullName}
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant='caption'>
                                                {userCurrentProfile.biography}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>

                                <Grid item xs={4} md={4} lg={4} alignItems={'center'} alignContent={'center'}
                                    alignSelf={'center'} justifyItems={'center'}>
                                    <ButtonGroup>
                                        {userCurrentProfile.isUserFollowedByUserRequesting ? (
                                            <Button
                                                className={'SpacedButton'}
                                                variant='contained'
                                                color='error'
                                                onClick={() => {
                                                    unfollowUser(user.userId, userCurrentProfile.id);
                                                }}
                                            >
                                                Ne plus suivre
                                            </Button>
                                        ) : (
                                            <Button
                                                className={'SpacedButton'}
                                                variant='contained'
                                                color='primary'
                                                onClick={() => {
                                                    followUser(user.userId, userCurrentProfile.id);
                                                }}
                                            >
                                                Suivre
                                            </Button>
                                        )}
                                        {/*<Button*/}
                                        {/*    className={'SpacedButton'}*/}
                                        {/*    variant='outlined'*/}
                                        {/*    // color=''*/}
                                        {/*    onClick={() => {*/}
                                        {/*        alert('TODO : sendPrivateMessage(userCurrentProfile.userId)');*/}
                                        {/*    }}*/}
                                        {/*>*/}
                                        {/*    <MailOutlineIcon />*/}
                                        {/*</Button>*/}
                                    </ButtonGroup>
                                </Grid>
                            </Grid>
                            <Grid container>

                            </Grid>
                        </Grid>

                        <header className='FeedSection'>
                            {!userProfilePosts && <CircularProgress className='CircularLoader' />}
                            {userProfilePosts && userProfilePosts.length === 0 && (
                                <Typography className='NoPostFound' variant='h6' style={{ marginTop: 10 }}>
                                    Cet utilisateur n'a aucun post !
                                </Typography>
                            )}
                            {userProfilePosts && userProfilePosts.length > 0 &&
                                userProfilePosts.map((post) => {
                                    return (
                                        <Card variant='outlined' className='PostContainer'>
                                            <Grid container flexDirection={'row'}>
                                                <Grid
                                                    container
                                                    xs={1}
                                                    md={1}
                                                    lg={1}
                                                    flexDirection={'column'}
                                                    alignItems={'center'}
                                                    justifyContent={'center'}
                                                    alignContent={'center'}
                                                    justifyItems={'center'}
                                                    paddingRight='10px'
                                                >
                                                    <Button
                                                        className='ButtonWithoutBackgroundAndBorders'
                                                        onClick={() => {
                                                            if (!post.hasUserUpVoted) {
                                                                voteOnPost(post, user, true);
                                                            }
                                                        }}
                                                    >
                                                        <KeyboardArrowUpIcon />
                                                    </Button>
                                                    <Typography
                                                        id={'post' + post.id + 'score'}
                                                        variant='h6'
                                                        color={
                                                            post.hasUserUpVoted
                                                                ? 'green'
                                                                : post.hasUserDownVoted
                                                                    ? 'red'
                                                                    : 'black'
                                                        }
                                                    >
                                                        {post.score}
                                                    </Typography>
                                                    <Button
                                                        className='ButtonWithoutBackgroundAndBorders'
                                                        onClick={() => {
                                                            if (!post.hasUserDownVoted) {
                                                                voteOnPost(post, user, false);
                                                            }
                                                        }}
                                                    >
                                                        <KeyboardArrowDownIcon />
                                                    </Button>
                                                </Grid>
                                                <Grid xs={11} md={11} lg={11}>
                                                    <ButtonBase
                                                        className='ClickableCard'
                                                        onClick={() => goToPostDetails(post.id)}
                                                    >
                                                        <React.Fragment>
                                                            <Button
                                                                size='large'
                                                                sx={{
                                                                    fontSize: 14,
                                                                    color: 'text.secondary',
                                                                }}
                                                                onClick={() => {
                                                                    seeAuthor(
                                                                        post.author.id,
                                                                    );
                                                                }}
                                                                style={{ margin: 'auto' }}
                                                            >
                                                                @
                                                                {post?.author.pseudo ??
                                                                    `Post's author`}
                                                                {' - '}
                                                                {Duration.timeSinceFrench(
                                                                    post.createdAt,
                                                                )}
                                                            </Button>
                                                            <CardContent className='PostContent'>
                                                                <Typography
                                                                    variant='h5'
                                                                    color='text.secondary'
                                                                    gutterBottom
                                                                >
                                                                    {post.title}
                                                                </Typography>
                                                                <Typography component='div' align='left'>
                                                                    {post.content}
                                                                </Typography>
                                                                <br />
                                                                {post.codes.length > 0 &&
                                                                    post.codes.map((code) => {
                                                                        return (
                                                                            <>
                                                                                <SyntaxHighlighter
                                                                                    style={theme}
                                                                                    className='HighlightedLanguage'
                                                                                >
                                                                                    {
                                                                                        `/** Langage utilisé : ${code.language} **/`
                                                                                    }
                                                                                </SyntaxHighlighter>
                                                                                <SyntaxHighlighter
                                                                                    language={
                                                                                        code.langage
                                                                                    }
                                                                                    style={theme}
                                                                                    className='HighlightedCode'
                                                                                >
                                                                                    {code.code}
                                                                                </SyntaxHighlighter>
                                                                            </>
                                                                        );
                                                                    })}
                                                            </CardContent>
                                                        </React.Fragment>
                                                    </ButtonBase>
                                                    <CardActions>
                                                        <Button
                                                            size='large'
                                                            onClick={() => openSandbox(post.codes[0].code, post.codes[0].language)}
                                                            style={{ margin: 'auto' }}
                                                        >
                                                            Ouvrir une sandbox {'< />'}
                                                        </Button>
                                                    </CardActions>
                                                    <CardContent className='PostContent'>
                                                        <Typography component='div' align='left'>
                                                            {post.countComments || 0} commentaires
                                                        </Typography>
                                                    </CardContent>
                                                </Grid>
                                            </Grid>
                                        </Card>
                                    );
                                })}
                        </header>
                    </Grid>

                    <Grid
                        item
                        xs={2}
                        md={2}
                        lg={2}
                        borderLeft='1px solid lightgrey'
                        style={{ paddingTop: '30px' }}
                    >
                        <header className='FriendsSection'>
                            <h3>Abonnements</h3>
                            <Followings
                                userId={user.userId}
                                email={user.email}
                                pseudo={user.pseudo}
                                firstName={user.firstName}
                                fullName={user.fullName}
                                lastName={user.lastName}
                            />
                        </header>
                    </Grid>
                </Grid>
            )}
        </div>
    );
}