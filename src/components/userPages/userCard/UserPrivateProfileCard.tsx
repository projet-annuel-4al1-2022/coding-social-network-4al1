import * as React from 'react';
import { useEffect, useState } from 'react';
import './UserPrivateProfileCard.css';
import InputAdornment from '@mui/material/InputAdornment';
import EmailIcon from '@mui/icons-material/Email';
import KeyIcon from '@mui/icons-material/Key';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import PersonIcon from '@mui/icons-material/Person';
import BadgeIcon from '@mui/icons-material/Badge';
import UserService from '../../../services/user.service';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Button from '@mui/material/Button';
import { UserAPIService } from '../UserAPIService';
import { CircularProgress } from '@mui/material';
import DescriptionIcon from '@mui/icons-material/Description';

export function UserPrivateProfileCard() {
    const user = UserService.getCurrentUser();

    const [currentUser, setCurrentUser] = useState(null);

    const getCurrentUserProfile = async () => {
        const currentUser = await UserAPIService.getUserProfile(user.userId);
        console.log('Retrieved current user ', currentUser);
        setCurrentUser(currentUser);
    };

    useEffect(() => {
        getCurrentUserProfile();
    }, []);

    const [showPassword, setPasswordShown] = useState(false);

    async function update(event) {
        event.preventDefault();
        UserAPIService.updateProfile(currentUser.id, {
            email: event.target.email.value,
            pseudo: event.target.pseudo.value,
            firstName: event.target.firstName.value,
            lastName: event.target.lastName.value,
            password: event.target.password.value,
        }).then(() => {
            window.location.reload();
        });
    }

    function handlePasswordChange(inputType?: string) {
        if (inputType === 'password') {
            setPasswordShown(false);
        } else {
            setPasswordShown(!showPassword);
        }
    }

    return (
        <>
            {!currentUser && <CircularProgress className='CircularLoader' />}
            {currentUser && (
                <div className='UserCard'>
                    <div className='upper-container'>
                        <div className='image-container'>
                            <img
                                src={require('../../../images/user-image.png')}
                                alt=''
                                height='100px'
                                width='100px'
                            />
                        </div>
                        <div className='lower-container'>
                            <h3> @{currentUser.pseudo} </h3>
                            <div>
                                <Box
                                    sx={{
                                        margin: 5,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    className='form-container'
                                    component='form'
                                    noValidate
                                    onSubmit={(event) => {
                                        update(event);
                                    }}
                                >
                                    <Grid container spacing={1}>
                                        <Grid item xs={10}>
                                            <TextField
                                                size='small'
                                                id='standard-basic'
                                                variant='standard'
                                                label='Prénom'
                                                autoComplete='first-name'
                                                name='firstName'
                                                fullWidth
                                                autoFocus
                                                defaultValue={currentUser.firstName}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position='start'>
                                                            <PersonIcon />
                                                        </InputAdornment>
                                                    ),
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={10}>
                                            <TextField
                                                size='small'
                                                id='standard-basic'
                                                variant='standard'
                                                label='Nom'
                                                autoComplete='last-name'
                                                name='lastName'
                                                fullWidth
                                                defaultValue={currentUser.lastName}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position='start'>
                                                            <BadgeIcon />
                                                        </InputAdornment>
                                                    ),
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={10}>
                                            <TextField
                                                size='small'
                                                id='standard-basic'
                                                variant='standard'
                                                label='Pseudo'
                                                autoComplete='pseudo'
                                                aria-readonly
                                                name='pseudo'
                                                fullWidth
                                                defaultValue={currentUser.pseudo}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position='start'>
                                                            <BadgeIcon />
                                                        </InputAdornment>
                                                    ),
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={10}>
                                            <TextField
                                                size='small'
                                                id='standard-basic'
                                                variant='standard'
                                                label='Biographie'
                                                autoComplete='biography'
                                                aria-readonly
                                                name='biography'
                                                fullWidth
                                                multiline
                                                defaultValue={currentUser.biography}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position='start'>
                                                            <DescriptionIcon />
                                                        </InputAdornment>
                                                    ),
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={10}>
                                            <TextField
                                                size='small'
                                                fullWidth
                                                id='standard-basic'
                                                variant='standard'
                                                label='Email'
                                                name='email'
                                                defaultValue={currentUser.email}
                                                autoComplete='email'
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position='start'>
                                                            <EmailIcon />
                                                        </InputAdornment>
                                                    ),
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={10}>
                                            <TextField
                                                size='small'
                                                id='standard-basic'
                                                variant='standard'
                                                fullWidth
                                                name='password'
                                                label='Mot de passe'
                                                autoComplete='password'
                                                placeholder={'*********'}
                                                type={
                                                    showPassword ? 'text' : 'password'
                                                }
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position='start'>
                                                            <KeyIcon />
                                                        </InputAdornment>
                                                    ),
                                                    endAdornment: (
                                                        <InputAdornment position='end'>
                                                            <VisibilityIcon
                                                                onClick={() => {
                                                                    handlePasswordChange();
                                                                }}
                                                            />
                                                        </InputAdornment>
                                                    ),
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={8}>
                                            <div
                                                id='sign-up-error'
                                                className='FormError'
                                            />
                                        </Grid>
                                    </Grid>
                                    <Button
                                        type='submit'
                                        id='update'
                                        variant='contained'
                                    >
                                        Mettre à jour
                                    </Button>
                                </Box>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
}
