import { ApiService } from '../../services/api.service';
import { UpdateUserDto } from '../../models/dto/UpdateUser.dto';

export class UserAPIService {
    // update User profile
    static async updateProfile(userId: number, updateUserDto: UpdateUserDto) {
        const updateProfileUrl = `${process.env.REACT_APP_API_URL}/users/${userId}`;
        return ApiService.patch(updateProfileUrl, {
            ...updateUserDto,
        });
    }

    static async getUserProfile(userId: number, userRequestingId?: number): Promise<any> {
        const getUserProfileApiUrl = `${
            process.env.REACT_APP_API_URL
        }/users/${userId}?userRequestingId=${userRequestingId}`;
        return ApiService.get(getUserProfileApiUrl);
    }

    static followUser(userId, userIdToFollow) {
        const followUserUrl = `${process.env.REACT_APP_API_URL}/users/${userId}/follow/${userIdToFollow}`;
        return ApiService.post(followUserUrl);
    }

    static unfollowUser(userId, userIdToFollow) {
        const unfollowUserUrl = `${process.env.REACT_APP_API_URL}/users/${userId}/unfollow/${userIdToFollow}`;
        return ApiService.post(unfollowUserUrl);
    }

    static searchUser(value: string) {
        const searchUserUrl = `${process.env.REACT_APP_API_URL}/users/search?pseudo=${value}`;
        return ApiService.get(searchUserUrl);
    }
}