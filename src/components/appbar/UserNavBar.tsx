import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import { AccountCircle } from '@mui/icons-material';
import { GenericNotFound } from '../errors/GenericNotFound';
import { goToDashboard, goToUserPosts, goToUserProfile, goToUserSkills, logout } from '../../services/routing.service';
import { ColorService } from '../../services/color.service';

const pages = ['Profil', 'Posts']; // Skills
const settings = ['Accueil', 'Déconnexion'];

export default function UserNavBar() {
    const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
        null,
    );
    const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
        null,
    );

    const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const navListener = (page) => {
        if (page === 'Profil') {
            goToUserProfile();
        } else if (page === 'Posts') {
            goToUserPosts();
        } else if (page === 'Skills') {
            goToUserSkills();
        } else {
            <GenericNotFound />;
        }
    };

    const navMenu = (setting) => {
        if (setting === 'Accueil') {
            goToDashboard();
        } else if (setting === 'Déconnexion') {
            logout();
        } else {
            <GenericNotFound />;
        }
    };

    return (
        <Box sx={{ display: 'flex' }} color={'red'}>
            <CssBaseline />
            <AppBar
                style={{ background: ColorService.APP_PRIMARY_COLOR }}
                position='fixed'
                sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
            >
                <Container maxWidth='xl'>
                    <Toolbar disableGutters>
                        <Typography
                            variant='h6'
                            noWrap
                            component='div'
                            sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
                        >
                            <a
                                href='/'
                                // onClick={() => goToDashboard()}
                                className='LinkWithoutFormat Bold'
                            >
                                CSN
                            </a>
                        </Typography>

                        <Box
                            sx={{
                                flexGrow: 1,
                                display: { xs: 'flex', md: 'none' },
                            }}
                        >
                            <IconButton
                                size='large'
                                aria-label='account of current user'
                                aria-controls='menu-appbar'
                                aria-haspopup='true'
                                onClick={handleOpenNavMenu}
                                color='inherit'
                            >
                                <MenuIcon />
                            </IconButton>
                            <Menu
                                id='menu-appbar'
                                anchorEl={anchorElNav}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                sx={{
                                    display: { xs: 'block', md: 'none' },
                                }}
                            >
                                {pages.map((page) => (
                                    <MenuItem
                                        key={page}
                                        onClick={() => navListener(page)}
                                    >
                                        <Typography textAlign='center'>
                                            {page}
                                        </Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>
                        <Typography
                            variant='h6'
                            noWrap
                            component='div'
                            sx={{
                                flexGrow: 1,
                                display: { xs: 'flex', md: 'none' },
                            }}
                        >
                            <a
                                href='/'
                                // onClick={() => goToDashboard()}
                                className='LinkWithoutFormat Bold'
                            >
                                CSN
                            </a>
                        </Typography>

                        <Box
                            sx={{
                                flexGrow: 1,
                                display: { xs: 'none', md: 'flex' },
                            }}
                        >
                            {pages.map((page) => (
                                <Button
                                    key={page}
                                    onClick={() => navListener(page)}
                                    sx={{
                                        my: 2,
                                        color: 'white',
                                        display: 'block',
                                    }}
                                >
                                    {page}
                                </Button>
                            ))}
                        </Box>

                        <Box sx={{ flexGrow: 0 }}>
                            <Tooltip title='Cliquez !'>
                                <IconButton
                                    onClick={handleOpenUserMenu}
                                    sx={{ p: 0 }}
                                    color='inherit'
                                >
                                    <AccountCircle />
                                </IconButton>
                            </Tooltip>
                            <Menu
                                sx={{ mt: '45px' }}
                                id='menu-appbar'
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {settings.map((setting) => (
                                    <>
                                        {setting === 'Déconnexion' ? (
                                            <MenuItem
                                                key={setting} onClick={() => navMenu(setting)}
                                                className='Button LogoutButton Bold'>
                                                <Typography textAlign='center'>
                                                    {setting}
                                                </Typography>
                                            </MenuItem>


                                        ) : (
                                            <MenuItem
                                                key={setting}
                                                onClick={() => navMenu(setting)}
                                                className='Button Bold'
                                            >
                                                <Typography textAlign='center'>
                                                    {setting}
                                                </Typography>
                                            </MenuItem>
                                        )}
                                    </>
                                ))}
                            </Menu>
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>
        </Box>
    );
}
