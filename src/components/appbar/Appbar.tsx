import { alpha, styled } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import Badge from '@mui/material/Badge';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import AccountCircle from '@mui/icons-material/AccountCircle';
import AccountCircleTwoToneIcon from '@mui/icons-material/AccountCircleTwoTone';
import MailIcon from '@mui/icons-material/Mail';
import NotificationsIcon from '@mui/icons-material/Notifications';
import MoreIcon from '@mui/icons-material/MoreVert';
import React, { useEffect, useState } from 'react';
import {
    goToChallengeDetails,
    goToOtherUserProfile,
    goToPostDetailsFromSearch,
    goToUserProfile,
    logout,
} from '../../services/routing.service';
import UserService from '../../services/user.service';
import { ColorService } from '../../services/color.service';
import { UserAPIService } from '../userPages/UserAPIService';
import debounce from 'lodash.debounce';
import { Divider, List, ListItem, ListItemText } from '@mui/material';
import { PostService } from '../dashboard/posts/Post.service';
import { socket } from '../../services/web-socket.service';
import { getChallengedUserInChallengeInfos } from '../collaborative/challenges/Challenge.service';
import ChallengeNotificationService from '../../services/challenge-notification.service';

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
    },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
}));

export function PrimarySearchAppBar() {
    const user = UserService.getCurrentUser();

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] =
        React.useState<null | HTMLElement>(null);

    const [searchedUsers, setSearchedUsers] = useState(null);
    const [searchedPosts, setSearchedPosts] = useState(null);

    const nbPrivateMessages = 0;
    const [nbNotifications, setNbNotifications] = useState(0);
    const [receivedChallengeNotifications, setReceivedChallengeNotifications] = useState([]);
    const [showNotifications, setShowNotifications] = useState(false);
    // Clicking everywhere on the page makes showNotifications false

    useEffect(() => {
        const cachedChallengeNotifications = ChallengeNotificationService.getUserChallengeNotifications(user.id);
        setReceivedChallengeNotifications(cachedChallengeNotifications);
        setNbNotifications(cachedChallengeNotifications.length);
    }, []);

    useEffect(() => {
        socket.on('receive_challenge', (data) => {
            ChallengeNotificationService.addUserChallengeNotification(
                data,
                user.id,
            );
            const cachedChallengeNotifications = ChallengeNotificationService.getUserChallengeNotifications(user.id);
            console.log('All notifications challenge');
            console.log(cachedChallengeNotifications);

            setReceivedChallengeNotifications([...cachedChallengeNotifications]);
            setNbNotifications(nbNotifications + 1);
        });
    }, []);

    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };

    const handleMenuClose = () => {
        setAnchorEl(null);
        handleMobileMenuClose();
    };

    const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
        setMobileMoreAnchorEl(event.currentTarget);
    };

    const debouncedSearchUserAndPost = debounce((query) => {
        searchUserAndPost(query);
    }, 500);

    const searchUserAndPost = async (query) => {
        if (query.length > 0) {
            const foundUsers = await UserAPIService.searchUser(query);
            setSearchedUsers(foundUsers);
            const foundPosts = await PostService.searchPosts(query);
            console.log('foundPosts', foundPosts);
            setSearchedPosts(foundPosts);
        } else {
            setSearchedUsers(null);
            setSearchedPosts(null);
        }
    };

    const goToChallenge = (receivedChallenge) => {
        ChallengeNotificationService.removeUserChallengeNotification(receivedChallenge, user.id);
        goToChallengeDetails(receivedChallenge.id);
    };

    const menuId = 'primary-search-account-menu';
    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            id={menuId}
            keepMounted
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem onClick={goToUserProfile} className='Button Bold'>
                Profil
            </MenuItem>
            <MenuItem onClick={logout} className='Button LogoutButton Bold'>
                Déconnexion
            </MenuItem>
        </Menu>
    );

    const mobileMenuId = 'primary-search-account-menu-mobile';
    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            id={mobileMenuId}
            keepMounted
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
        >
            <MenuItem>
                <IconButton
                    size='large'
                    aria-label={
                        'Show ' + nbNotifications + ' new notification(s)'
                    }
                    color='inherit'
                >
                    <Badge badgeContent={nbNotifications} color='error'>
                        <NotificationsIcon />
                    </Badge>
                </IconButton>
                <p>Notifications</p>
            </MenuItem>
            <MenuItem onClick={handleProfileMenuOpen}>
                <IconButton
                    size='large'
                    aria-label='account of current user'
                    aria-controls='primary-search-account-menu'
                    aria-haspopup='true'
                    color='inherit'
                >
                    <AccountCircleTwoToneIcon />
                </IconButton>
                <p>Profil</p>
            </MenuItem>
        </Menu>
    );

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar
                position='static'
                style={{ background: ColorService.APP_PRIMARY_COLOR }}
            >
                <Toolbar>
                    <Typography
                        variant='h5'
                        noWrap
                        component='div'
                        sx={{
                            display: { xs: 'none', sm: 'block' },
                            fontWeight: 'bold',
                        }}
                    >
                        <a
                            href='/'
                            // onClick={() => goToDashboard()}
                            className='LinkWithoutFormat Bold'
                        >
                            CSN
                        </a>
                    </Typography>
                    <Search style={{ alignItems: 'center' }}>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder='Chercher...'
                            inputProps={{ 'aria-label': 'search' }}
                            onChange={(event) => debouncedSearchUserAndPost(event.target.value)}
                            onBlur={() => {
                                // Wait to click event to be fired
                                setTimeout(() => {
                                    setSearchedUsers(null);
                                    setSearchedPosts(null);
                                }, 100);
                            }}
                        />
                        {/*    Display search result of user search with react mui*/}
                        {((searchedUsers && searchedUsers.length > 0) || (searchedPosts && searchedPosts.length > 0)) && (
                            <div className='search-results'>
                                {searchedUsers && searchedUsers.length > 0 && (
                                    <List>
                                        {searchedUsers.map((user) => (
                                            <ListItem
                                                key={user.id}
                                                button
                                                onClick={() => {
                                                    console.log('Clicked : goToOtherUserProfile');
                                                    goToOtherUserProfile(user.id);
                                                }}
                                            >
                                                <ListItemText primary={'@' + user.pseudo} />
                                            </ListItem>
                                        ))}
                                    </List>
                                )}
                                {/* Set color to divider */}
                                <Divider variant='fullWidth' />

                                {searchedPosts && searchedPosts.length > 0 && (
                                    <List>
                                        {searchedPosts.map((post) => (
                                            <ListItem
                                                key={post.id}
                                                button
                                                onClick={() => {
                                                    console.log('Clicked : goToPostDetails');
                                                    goToPostDetailsFromSearch(post.id);
                                                }}
                                            >
                                                <ListItemText primary={post.title} />
                                            </ListItem>
                                        ))}
                                    </List>
                                )}
                            </div>
                        )}
                    </Search>

                    <Box sx={{ flexGrow: 1 }} />
                    <Typography
                        variant='subtitle1'
                        noWrap
                        component='div'
                        sx={{ display: { xs: 'none', sm: 'block' } }}
                    >
                        {user.email}
                    </Typography>
                    <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
                        <IconButton
                            size='large'
                            aria-label={
                                'Show ' +
                                nbPrivateMessages +
                                ' new messages(s)'
                            }
                            color='inherit'
                        >
                            <Badge
                                badgeContent={nbPrivateMessages}
                                color='error'
                            >
                                <MailIcon />
                            </Badge>
                        </IconButton>
                        <IconButton
                            size='large'
                            aria-label={
                                'Show ' +
                                nbNotifications +
                                ' new notification(s)'
                            }
                            color='inherit'
                            onClick={() => {
                                if (showNotifications) {
                                    setShowNotifications(false);
                                } else {
                                    setShowNotifications(true);
                                }
                            }}
                        >
                            <Badge badgeContent={nbNotifications} color='error'>
                                <NotificationsIcon />
                            </Badge>
                        </IconButton>
                        {showNotifications && (
                            <div className='notifications' onMouseLeave={() => setShowNotifications(false)}>
                                {receivedChallengeNotifications && receivedChallengeNotifications.length > 0 && (
                                    <List>
                                        {receivedChallengeNotifications.map((receivedChallenge, index) => (
                                            <ListItem
                                                key={index}
                                                button
                                                onClick={() => {
                                                    console.log('Clicked : goToOtherUserProfile');
                                                    goToChallenge(receivedChallenge);
                                                }}
                                            >
                                                <ListItemText
                                                    primary={'Challenge "' + receivedChallenge.exercise.name + '" de la part de @' + getChallengedUserInChallengeInfos(receivedChallenge)?.user?.pseudo} />
                                            </ListItem>
                                        ))}
                                    </List>
                                )}
                                {receivedChallengeNotifications && receivedChallengeNotifications.length <= 0 && (
                                    <Typography>
                                        Aucune notification
                                    </Typography>
                                )}
                            </div>
                        )}
                        <IconButton
                            size='large'
                            edge='end'
                            aria-label='account of current user'
                            aria-controls={menuId}
                            aria-haspopup='true'
                            onClick={handleProfileMenuOpen}
                            color='inherit'
                        >
                            <AccountCircle />
                        </IconButton>
                    </Box>
                    <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
                        <IconButton
                            size='large'
                            aria-label='show more'
                            aria-controls={mobileMenuId}
                            aria-haspopup='true'
                            onClick={handleMobileMenuOpen}
                            color='inherit'
                        >
                            <MoreIcon />
                        </IconButton>
                    </Box>
                </Toolbar>
            </AppBar>
            {renderMobileMenu}
            {renderMenu}
        </Box>
    );
}
