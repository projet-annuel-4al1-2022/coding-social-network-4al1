import React from 'react';
import '../dashboard/Dashboard.css';
import { Button, Grid } from '@mui/material';
import { goToExercises, goToUserOnGoingChallenges } from '../../services/routing.service';

export function Header({
    pageTitle,
    leftButtonFunction,
    leftButtonContent,
}) {
    return (
        <Grid container borderBottom={'1px solid lightgrey'}>
            <Grid
                container
                direction='row'
                justifyContent='flex-start'
                alignItems='center'
            >
                <Grid
                    container
                    flexDirection='row'
                    justifyContent='flex-start'
                    alignItems='center'
                >
                    <Grid item>
                        <Button
                            className='Button SnippetsButton'
                            variant='contained'
                            color='secondary'
                            onClick={leftButtonFunction}
                        >
                            {leftButtonContent}
                        </Button>
                        {/* Algorithms Challenges */}
                        <Button
                            className='Button ChallengesButton'
                            variant='contained'
                            style={{ marginRight: '1rem' }}
                            onClick={() => {
                                goToUserOnGoingChallenges();
                            }}
                        >
                            Challenges
                        </Button>
                        <Button
                            className='Button ExercisesButton'
                            variant='contained'
                            color={'success'}
                            style={{ marginRight: '1rem' }}
                            onClick={() => {
                                goToExercises();
                            }}
                        >
                            Exercices
                        </Button>
                    </Grid>
                </Grid>
            </Grid>

            <Grid item xs={12} md={12} lg={12} sx={{ margin: 1 }}>
                {pageTitle && (
                    <header className='Header'>
                        <h1>{pageTitle}</h1>
                    </header>
                )}
            </Grid>
        </Grid>
    );
}
