export enum Difficulty {
    NOOB = 'NOOB',
    EASY = 'EASY',
    MEDIUM = 'MEDIUM',
    HARD = 'HARD',
    EXPERT = 'EXPERT',
}

export enum Language {
    C = 'c',
    CPLUSPLUS = 'c_cpp',
    JAVA = 'java',
    CSHARP = 'csharp',
    PYTHON = 'python',
}

export class Exercise {
    id: number;
    name: string;
    instructions: string;
    difficulty: Difficulty;
    language: Language;
    defaultCode: string;
}
