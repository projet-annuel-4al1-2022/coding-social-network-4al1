import { ApiService } from '../../../services/api.service';
import { Exercise } from '../entities/exercise.model';
import { User } from '../../../models/User.model';

export class ChallengeService {
    static async getExercises(): Promise<Exercise[]> {
        const getAllExercisesUrl = `${process.env.REACT_APP_API_URL}/exercises`;
        return ApiService.get<Exercise[]>(getAllExercisesUrl);
    }

    static async getExercise(exerciseId: number): Promise<Exercise> {
        const getExerciseUrl = `${process.env.REACT_APP_API_URL}/exercises/${exerciseId}`;
        return ApiService.get<Exercise>(getExerciseUrl);
    }

    static async getChallengers(exerciseId: number, userId: number) {
        const getChallengersUrl = `${process.env.REACT_APP_API_URL}/challenges/with-exercise/${exerciseId}/mutuals/${userId}`;
        return ApiService.get<User>(getChallengersUrl);
    }

    static async challengeUser(challengedId: number, userId, exerciseId) {
        const challengeUserUrl = `${process.env.REACT_APP_API_URL}/challenges`;
        return ApiService.post<any>(challengeUserUrl, {
            challengerId: userId,
            challengedId,
            exerciseId,
        });
    }

    static async getOnGoingChallenges(userId: number) {
        const getOnGoingChallengesUrl = `${process.env.REACT_APP_API_URL}/challenges/on-going/${userId}`;
        return ApiService.get(getOnGoingChallengesUrl);
    }

    static async getChallengeDetails(challengeId: string) {
        const getChallengeDetailsUrl = `${process.env.REACT_APP_API_URL}/challenges/${challengeId}`;
        return ApiService.get<any>(getChallengeDetailsUrl);
    }

    static submitExercise(exercise, userId, challengeId: string, codeContent: string, codeLanguage: string, numberOfValidatedTests: number) {
        if (exercise.language != codeLanguage) {
            alert('Le langage utilisé ne correspond pas au langage de l\'exercice');
            return;
        }
        const submitExerciseUrl = `${process.env.REACT_APP_API_URL}/challenges/${challengeId}/submit`;
        return ApiService.post(submitExerciseUrl, {
            codeContent,
            codeLanguage,
            numberOfValidatedTests,
            userId,
        });
    }

    static updateTimePassedOnChallenge(challengeId: string, userId: number, timePassedOnExerciseInSeconds: number) {
        const updateTimePassedOnChallengeUrl = `${process.env.REACT_APP_API_URL}/challenges/${challengeId}/time-passed`;
        return ApiService.patch(updateTimePassedOnChallengeUrl, {
            userId,
            challengeId,
            timePassedOnExerciseInSeconds,
        });
    }

    static uploadCode(userId: number, challengeId: number, sourceCode: string, sourceLanguage: string) {
        const uploadCodeUrl = `${process.env.REACT_APP_API_URL}/challenges/${challengeId}/upload`;
        return ApiService.post(uploadCodeUrl, {
            userId,
            code: sourceCode,
            language: sourceLanguage,
        });
    }

    static updateUserCodeExecutingTimeAndMemory(userId: number, challengeId: number, executionTime: number, usedMemory: number) {
        const updateUserCodeExecutingTimeAndMemoryUrl = `${process.env.REACT_APP_API_URL}/challenges/${challengeId}/user/${userId}`;
        return ApiService.patch(updateUserCodeExecutingTimeAndMemoryUrl, {
            executionTime,
            usedMemory,
        });
    }

    static getMutualsRankingSortedByAvgScore(userId: number) {
        const getMutualsRankingSortedByAvgScoreUrl = `${process.env.REACT_APP_API_URL}/challenges/mutuals/ranking/${userId}`;
        return ApiService.get(getMutualsRankingSortedByAvgScoreUrl);
    }

    static async getUserChallengesIsChallenger(userId: number) {
        const getUserChallengesUrl = `${process.env.REACT_APP_API_URL}/challenges/userChallenger/${userId}`;
        return ApiService.get<any>(getUserChallengesUrl);
    }

    static async getUserChallengesIsChallenged(userId: number) {
        const getUserChallengesUrl = `${process.env.REACT_APP_API_URL}/challenges/userChallenged/${userId}`;
        return ApiService.get<any>(getUserChallengesUrl);
    }
}

export function getChallengedUserInChallengeInfos(challenge) {
    return challenge.challengeInfos.find(challengeInfo => challengeInfo.isChallenger == false);
}

export function getChallengerUserInChallengeInfos(challenge) {
    return challenge.challengeInfos.find(challengeInfo => challengeInfo.isChallenger == true);
}

export function getCurrentUserChallengeInfos(challenge, userId: string) {
    return challenge.challengeInfos.find(challengeInfo => challengeInfo.user.id == userId);
}

export function getOpponentChallengeInfos(challenge, userId: string) {
    return challenge.challengeInfos.find(challengeInfo => challengeInfo.user.id != userId);
}
