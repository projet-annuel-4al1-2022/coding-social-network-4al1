import React, { useEffect, useState } from 'react';
import '../../../App.css';
import './Exercises.css';
import { ButtonBase, Card, CardContent, CircularProgress, Grid, Typography } from '@mui/material';
import { Header } from '../../header/Header';
import { goToExerciseDetails, goToSnippets } from '../../../services/routing.service';
import { ChallengeService } from './Challenge.service';

export function Exercises() {
    const [exercises, setExercises] = useState(null);

    const getExercises = async () => {
        const exercises = await ChallengeService.getExercises();
        console.log('Exercises: ', exercises);
        setExercises(exercises);
    };

    useEffect(() => {
        getExercises();
    }, []);

    const challengeClicked = (challenge) => {
        goToExerciseDetails(challenge.id);
    };

    return (
        <div className='Exercises'>
            <Grid container>
                <Header
                    pageTitle={'Exercices'}
                    leftButtonContent={'Mes snippets'}
                    leftButtonFunction={goToSnippets}
                />

                <Grid item xs={12} md={12} lg={12}>
                    <Grid className='Exercices'>
                        {!exercises && <CircularProgress className='CircularLoader' />}
                        {exercises && exercises.length <= 0 && (
                            <Typography variant={'h6'}>
                                Il n'y a pas d'exercices disponibles
                            </Typography>
                        )}
                        {exercises && exercises.length > 0 && exercises.map(exercise => (
                            //    With material card
                            <ButtonBase sx={{ margin: 5 }} onClick={() => challengeClicked(exercise)} key={exercise.id}>
                                <Card sx={{ maxWidth: 345 }}>
                                    <CardContent>
                                        <Typography className='ExerciseName' variant='h5' component='h2'
                                            fontWeight={'bold'}>
                                            {exercise.name}
                                        </Typography>
                                        <Typography className='ExerciseInstructions' variant='body2' component='p'>
                                            {exercise.instructions}
                                        </Typography>
                                        <Typography className='ExerciseDifficultyAndLanguage' variant='subtitle1'
                                            component='h2' color={'primary'}>
                                            Difficulté : {exercise.difficulty} - {exercise.language}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </ButtonBase>
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}