import React, { useEffect, useState } from 'react';
import '../../../../App.css';
import './ExerciseDetails.css';
import { Button, CircularProgress, Grid, Typography } from '@mui/material';
import { Header } from '../../../header/Header';
import { goToExerciseResolution, goToSnippets } from '../../../../services/routing.service';
import UserService from '../../../../services/user.service';
import { ChallengeService } from '../Challenge.service';
import { useParams } from 'react-router-dom';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Dialog from '@mui/material/Dialog';
import { socket } from '../../../../services/web-socket.service';

export function ExerciseDetails() {
    const user = UserService.getCurrentUser();
    console.log('Current user: ' + JSON.stringify(user, null, 4));
    const [exercise, setExercise] = useState(null);
    const [candidatesToChallenge, setCandidatesToChallenge] = useState(null);
    const { exerciseId } = useParams();

    const [launchedChallenge, setLaunchedChallenge] = useState(false);

    const [dialogOpen, setDialogOpen] = useState(false);

    const handleClickOpen = () => {
        setDialogOpen(true);
    };

    const handleClose = () => {
        setDialogOpen(false);
    };

    const getExercise = async () => {
        const exercise = await ChallengeService.getExercise(parseInt(exerciseId));
        console.log('Exercise: ', exercise);
        setExercise(exercise);
    };

    const getChallengers = async () => {
        const challengers = await ChallengeService.getChallengers(parseInt(exerciseId), parseInt(user.id));
        console.log('Challengers: ', challengers);
        setCandidatesToChallenge(challengers);
    };

    useEffect(() => {
        getExercise();
        getChallengers();
    }, []);

    const challengeSomeone = async () => {
        // Open a dialog to ask for the user to select a user
        handleClickOpen();
    };

    const challengeUser = async (challengedUser) => {
        setLaunchedChallenge(true);
        if (challengedUser.hasBeenAlreadyChallengedByCurrentUserOnExercise) {
            const res = confirm(`Attention, vous avez déjà défié ${challengedUser.user.pseudo} sur cet exercice ! Voulez-vous vraiment défié ce joueur ?`);
            if (!res) {
                setLaunchedChallenge(false);
                return;
            }
        }
        // Close the dialog
        handleClose();

        // Challenge the user
        const challengedLaunched = await ChallengeService.challengeUser(challengedUser.user.id, user.id, exerciseId);

        // With socket.io, send a notification to the user that he has been challenged
        socket.emit('send_challenge', { data: JSON.stringify(challengedLaunched) });
        console.log('Challenged launched: ', challengedLaunched);
        setLaunchedChallenge(false);
        
        // Redirect to the challenge page
        goToExerciseResolution(challengedLaunched.id);
    };

    return (
        <div className='Exercise'>
            <Grid container>
                {(!exercise) && (
                    <>
                        <Header
                            pageTitle={'Exercice'}
                            leftButtonContent={'Mes snippets'}
                            leftButtonFunction={goToSnippets}
                        />
                        <div className='ExerciseNotLoaded'>
                            <CircularProgress className='CircularLoader' />
                        </div>
                    </>
                )}
                {launchedChallenge && (
                    <>
                        <div className='ExerciseNotLoaded'>
                            <CircularProgress className='CircularLoader' />
                        </div>
                    </>
                )}
                {exercise && (
                    <>
                        <Header
                            pageTitle={exercise.name}
                            leftButtonContent={'Mes snippets'}
                            leftButtonFunction={goToSnippets}
                        />
                        <Grid item xs={12} md={12} lg={12}>
                            <Grid className='Exercice'>
                                <Grid className='ExerciseName'>
                                    <Grid className='ExerciseInstructions'>
                                        <Typography variant='body2' component='p'>
                                            {exercise.instructions}
                                        </Typography>
                                    </Grid>
                                    <Grid className='ExerciseDifficultyAndLanguage'>
                                        <Typography variant='subtitle1' component='h2' fontStyle={'italic'}>
                                            Difficulté : {exercise.difficulty} - {exercise.language}
                                        </Typography>
                                    </Grid>
                                    {/* button to challenge someone in my followers and following */}
                                    <Grid className='Buttons'>
                                        <Grid className='Button'>
                                            <Button sx={{ margin: 5 }} variant={'contained'} color={'primary'}
                                                onClick={challengeSomeone}>
                                                Challengez quelqu'un
                                            </Button>

                                            <Dialog open={dialogOpen} onClose={handleClose} fullWidth={true}
                                                maxWidth={'lg'}>
                                                <DialogTitle>Choisir un adversaire</DialogTitle>
                                                <DialogContent>
                                                    <DialogContentText>
                                                        Vous serez noté sur la longueur, la rapidité (temps d'exécution)
                                                        de votre algorithme et le temps que vous prenez pour résoudre
                                                        l'exercice.
                                                        <br />
                                                        Choisissez un adversaire pour commencer à défier !
                                                    </DialogContentText>
                                                    <DialogContentText>
                                                        {/*  Challengers = followers INTER following  */}
                                                        {candidatesToChallenge && candidatesToChallenge.length > 0 && (
                                                            <>
                                                                {candidatesToChallenge.map((challenged) => (
                                                                    <div key={challenged.user.id}>
                                                                        <Button
                                                                            sx={{
                                                                                margin: 5,
                                                                            }}
                                                                            onClick={() => {
                                                                                challengeUser(challenged);
                                                                            }}
                                                                        >
                                                                            <Typography variant='body2' component='p'>
                                                                                - @{challenged.user.pseudo}
                                                                            </Typography>
                                                                        </Button>
                                                                    </div>
                                                                ))}
                                                            </>
                                                        )}
                                                        {candidatesToChallenge && candidatesToChallenge.length === 0 && (
                                                            <Typography variant='body2' component='p'>
                                                                Vous n'avez pas d'adversaires à défier (aucune personne
                                                                que
                                                                vous suivez ne vous suit)
                                                            </Typography>
                                                        )}
                                                    </DialogContentText>
                                                </DialogContent>
                                                <DialogActions>
                                                    <Button onClick={handleClose}>Annuler</Button>
                                                </DialogActions>
                                            </Dialog>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </>
                )}
            </Grid>
        </div>
    );
}