import React, { useEffect, useState } from 'react';
import '../../../../App.css';
import './UserOnGoingChallenges.css';
import { Button, ButtonBase, Card, CardContent, CircularProgress, Grid, Typography } from '@mui/material';
import { Header } from '../../../header/Header';
import { goToChallengeDetails, goToMyChallengesHistory, goToSnippets } from '../../../../services/routing.service';
import UserService from '../../../../services/user.service';
import { ChallengeService, getOpponentChallengeInfos } from '../Challenge.service';
import { ChallengeRanking } from './ranking/ChallengeRanking';
import { Link } from 'react-router-dom';


export function UserOnGoingChallenges() {
    const user = UserService.getCurrentUser();
    const [onGoingChallenges, setOnGoingChallenges] = useState(null);

    const getOnGoingChallenges = async () => {
        const onGoingChallenges = await ChallengeService.getOnGoingChallenges(user.id);
        console.log('On going challenges: ', onGoingChallenges);
        setOnGoingChallenges(onGoingChallenges);
    };

    useEffect(() => {
        getOnGoingChallenges();
    }, []);

    const challengeClicked = (challenge) => {
        goToChallengeDetails(challenge.id);
    };


    return (
        <div className='Challenges'>
            <Grid container>
                <Header
                    pageTitle={'Challenges en cours'}
                    leftButtonContent={'Mes snippets'}
                    leftButtonFunction={goToSnippets}
                />

                <Grid item xs={2} md={2} lg={2} borderRight='1px solid lightgrey'>
                    <header className='ClassementSection'>
                        <h3>Classement avec les mutuels</h3>
                        <ChallengeRanking />
                    </header>
                </Grid>

                <Grid item xs={8} md={8} lg={8} className='OnGoingChallenges' direction={'row'}>
                    {!onGoingChallenges && <CircularProgress className='CircularLoader' />}
                    {onGoingChallenges && onGoingChallenges.length <= 0 && (
                        <Typography variant={'h6'}>
                            Vous n'avez pas de challenge en cours
                        </Typography>
                    )}
                    {onGoingChallenges && onGoingChallenges.length > 0 && onGoingChallenges.map(challenge => (
                        <ButtonBase sx={{ margin: 5 }} onClick={() => challengeClicked(challenge)}>
                            <Card sx={{ maxWidth: 345 }}>
                                <CardContent>
                                    <Typography className='ExerciseName' variant='h5' component='h2'
                                        fontWeight={'bold'}>
                                        {challenge.exercise.name}
                                    </Typography>
                                    <Typography className='ExerciseDifficultyAndLanguage' variant='subtitle1'
                                        component='h2' color={'primary'}>
                                        Difficulté : {challenge.exercise.difficulty} - {challenge.exercise.language}
                                    </Typography>
                                    <Typography className='ChallengedUser' variant='body2' component='p'>
                                        Adversaire : @{getOpponentChallengeInfos(challenge, user.id).user.pseudo}
                                    </Typography>
                                    <Typography className='ChallengedUserHasFinished' variant='overline'
                                        component='p'>
                                        {getOpponentChallengeInfos(challenge, user.id).hasSubmittedExercise ? 'L\'adversaire a terminé' : 'L\'adversaire travaille encore...'}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </ButtonBase>
                    ))}
                </Grid>

                <Grid item xs={2} md={2} lg={2} borderLeft='1px solid lightgrey'>
                    <header className='ClassementSection'>
                        <h3>Historique des challenges</h3>
                        <Button onClick={goToMyChallengesHistory}>Accéder à l'historique</Button>
                    </header>
                </Grid>
                
            </Grid>

        </div>
    );
}

