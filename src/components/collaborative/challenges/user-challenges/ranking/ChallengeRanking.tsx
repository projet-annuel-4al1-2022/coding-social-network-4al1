import React, { useEffect, useState } from 'react';
import './ChallengeRanking.css';
import { ButtonBase, Card, Grid, Typography } from '@mui/material';
import { ChallengeService } from '../../Challenge.service';
import UserService from '../../../../../services/user.service';
import { seeAuthor } from '../../../../userPages/postsCard/UserPosts';

export function ChallengeRanking() {
    const user = UserService.getCurrentUser();
    const [rankings, setRankings] = useState(null);

    useEffect(() => {
        getMutualsRanking(user.userId);
    }, []);

    const getMutualsRanking = async (userId: number) => {
        const mutualsRanking = await ChallengeService.getMutualsRankingSortedByAvgScore(userId);
        console.log('Mutuals ranking: ', mutualsRanking);
        setRankings(mutualsRanking);
    };

    return (
        <>
            {rankings && rankings.length === 0 && (
                <Typography className='NoFollowingsFound' variant='h6'>
                    Vous ne suivez personne pour avoir un classement.
                </Typography>
            )}
            {rankings && rankings.length > 0 && rankings.map((ranking, index) => (
                <Card
                    variant='outlined'
                    className='RankingContainer'
                    key={index}
                >
                    <ButtonBase
                        sx={{
                            fontSize: 14,
                            color: 'text.primary',
                            fontWeight: 'bold',
                            width: '100%',
                        }}
                        onClick={() => seeAuthor(ranking.mutual?.id)}
                        // style={{ margin: 'auto' }}
                    >
                        <Grid container direction={'row'} alignItems={'center'} spacing={1}>
                            {/* Ranking index */}
                            <Grid item xs={2} md={2} lg={2} style={{ textAlign: 'left' }}>
                                <Typography
                                    className='RankingIndex'
                                    sx={{
                                        fontSize: 14,
                                        color: 'text.primary',
                                        fontWeight: 'bold',
                                    }}
                                    style={{ margin: 'auto' }}
                                >
                                    {index + 1}.
                                </Typography>
                            </Grid>
                            <Grid item xs={10} md={10} lg={10}>
                                {/* Display both element centered horizontally & vertically and direction column */}
                                <Grid container direction={'column'}>
                                    {/* User */}
                                    <Grid item xs={12}>
                                        <Typography
                                            className='Mutual'
                                            sx={{
                                                fontSize: 14,
                                                color: 'text.primary',
                                                fontWeight: 'bold',
                                            }}
                                            style={{ margin: 'auto' }}
                                        >
                                            @{ranking.mutual?.pseudo}
                                        </Typography>
                                    </Grid>
                                    {/* Score */}
                                    <Grid item xs={12}>
                                        <Typography
                                            className='Score'
                                            sx={{
                                                color: 'text.secondary',
                                            }}
                                        >
                                            {ranking.score ?? 0} points
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </ButtonBase>
                </Card>
            ))}

        </>
    );
}