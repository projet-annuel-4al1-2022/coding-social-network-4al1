import React, { useEffect, useState } from 'react';
import '../../../../../App.css';
import './UserChalllengeDetails.css';
import { Button, CircularProgress, Divider, Grid, Typography } from '@mui/material';
import { useParams } from 'react-router-dom';
import { goToExerciseResolution, goToSnippets } from '../../../../../services/routing.service';
import { Header } from '../../../../header/Header';
import {
    ChallengeService,
    getChallengedUserInChallengeInfos,
    getChallengerUserInChallengeInfos,
    getCurrentUserChallengeInfos,
} from '../../Challenge.service';
import UserService from '../../../../../services/user.service';
import { DurationService } from '../../../../../services/duration.service';

export function UserChallengeDetails() {
    const user = UserService.getCurrentUser();
    const [challenge, setChallenge] = useState(null);
    const [challengerChallengeInfo, setChallengerChallengeInfo] = useState(null);
    const [challengedChallengeInfo, setChallengedChallengeInfo] = useState(null);
    const { challengeId } = useParams();

    const getCurrentChallenge = async () => {
        const challenge = await ChallengeService.getChallengeDetails(challengeId);
        console.log('Current challenge: ', challenge);
        challenge.challengeInfos = challenge.challengeInfos.map((challengeInfo) => {
            challengeInfo.timeSpentOnExercise = DurationService.getFormattedSecondsInHourMinuteSeconds(challengeInfo.timeSpentOnExercise);
            return challengeInfo;
        });
        setChallengedChallengeInfo(getChallengedUserInChallengeInfos(challenge));
        setChallengerChallengeInfo(getChallengerUserInChallengeInfos(challenge));
        setChallenge(challenge);
    };

    useEffect(() => {
        getCurrentChallenge();
    }, []);

    return (
        <div className='ChallengeDetails'>
            <Grid container>
                {!challenge && (
                    <>
                        <Header
                            pageTitle={''}
                            leftButtonContent={'Mes snippets'}
                            leftButtonFunction={goToSnippets}
                        />
                        <div className='ChallengeNotLoaded'>
                            <CircularProgress className='CircularLoader' />
                        </div>
                    </>
                )}
                {challenge && (
                    <>
                        <Header
                            pageTitle={challenge.exercise.name}
                            leftButtonContent={'Mes snippets'}
                            leftButtonFunction={goToSnippets}
                        />
                        <Grid item xs={12} md={12} lg={12}>
                            <Grid className='Exercice' direction={'row'}>
                                <Grid className='ExerciseName' direction={'row'}>
                                    <Grid className='ExerciseInstructions' direction={'row'}>
                                        <br />
                                        <Typography variant='body2' component='p'>
                                            {challenge.exercise.instructions}
                                        </Typography>
                                    </Grid>
                                    <Grid className='ExerciseDifficultyAndLanguage' direction={'row'}>
                                        <Typography variant='subtitle1' component='h2' fontStyle={'italic'}>
                                            Difficulté : {challenge.exercise.difficulty} - {challenge.exercise.language}
                                        </Typography>
                                    </Grid>
                                    <Button variant='contained'
                                        disabled={getCurrentUserChallengeInfos(challenge, user.id).hasSubmittedExercise}
                                        className={'CapitalizedButton ForgiveButton'}
                                        onClick={() => goToExerciseResolution(challenge.id)}>
                                        Résoudre
                                    </Button>

                                    <Divider variant='middle' />

                                    <Grid container>
                                        <Grid item xs>
                                            <Grid className='ExerciseChallenger' direction={'row'}>
                                                <Grid className='ExerciseChallengerName' direction={'row'}>
                                                    <br />
                                                    <Typography variant='h4' component='h2'>
                                                        Challenger
                                                    </Typography>
                                                    <Typography variant='h6' component='h2'>
                                                        @{challengerChallengeInfo.user.pseudo}{challengerChallengeInfo.user.pseudo == user.pseudo ? ' (vous)' : ''}
                                                    </Typography>
                                                    {/*"score": 0*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Score :</strong> {challengerChallengeInfo.score}
                                                    </Typography>
                                                    {/*"hasSubmittedExercise": false,*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Soumis
                                                            :</strong> {challengerChallengeInfo.hasSubmittedExercise ? 'Oui' : 'Non'}
                                                    </Typography>
                                                    {/*"hasValidatedExercise": false,*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Validé
                                                            :</strong> {challengerChallengeInfo.hasValidatedExercise ? 'Oui' : 'Non'}
                                                    </Typography>
                                                    {/*"timeSpentOnExercise": 0,*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Temps passé
                                                            :</strong> {challengerChallengeInfo.timeSpentOnExercise}
                                                    </Typography>
                                                    {/*"executionTime": 0,*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Temps d'exécution moyen
                                                            :</strong> {Math.round(challengerChallengeInfo.executionTime * 1000) / 1000} secondes
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>

                                        <Divider orientation='vertical' flexItem>
                                        </Divider>

                                        <Grid item xs>

                                            <Grid className='ExerciseChallenged' direction={'row'}>
                                                <Grid className='ExerciseChallengedName' direction={'row'}>
                                                    <br />
                                                    <Typography variant='h4' component='h2'>
                                                        Challenged
                                                    </Typography>
                                                    <Typography variant='h6' component='h2'>
                                                        @{challengedChallengeInfo.user.pseudo}{challengedChallengeInfo.user.pseudo == user.pseudo ? ' (vous)' : ''}
                                                    </Typography>
                                                    {/*"score": 0*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Score :</strong> {challengedChallengeInfo.score}
                                                    </Typography>
                                                    {/*"hasSubmittedExercise": false,*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Soumis
                                                            :</strong> {challengedChallengeInfo.hasSubmittedExercise ? 'Oui' : 'Non'}
                                                    </Typography>
                                                    {/*"hasValidatedExercise": false,*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Validé
                                                            :</strong> {challengedChallengeInfo.hasValidatedExercise ? 'Oui' : 'Non'}
                                                    </Typography>
                                                    {/*"timeSpentOnExercise": 0,*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Temps passé
                                                            :</strong> {challengedChallengeInfo.timeSpentOnExercise}
                                                    </Typography>
                                                    {/*"executionTime": 0,*/}
                                                    <Typography variant='h6' component='h2'>
                                                        <strong>Temps d'exécution moyen
                                                            {/* Math round to 3 decimals */}
                                                            :</strong> {Math.round(challengedChallengeInfo.executionTime * 1000) / 1000} secondes
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </>
                )}
            </Grid>
        </div>
    );
}
