import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import './ResolveExercise.css';
import './../../../../App.css';
import UserService from '../../../../services/user.service';
import { Button, CircularProgress, Grid, Typography } from '@mui/material';
import { ChallengeService, getCurrentUserChallengeInfos } from '../Challenge.service';
import { goToChallengeDetails, goToSnippets } from '../../../../services/routing.service';
import { Header } from '../../../header/Header';
import { ExerciseResolutionEditor } from '../../../Editor/ExerciseResolution/ExerciseResolutionEditor';
import { DurationService } from '../../../../services/duration.service';

export function ResolveExercise() {
    const user = UserService.getCurrentUser();

    const [exercise, setExercise] = useState(null);
    const [challengeInfo, setChallengeInfo] = useState(null);
    const [codeContent, setCodeContent] = useState('');
    const [codeLanguage, setCodeLanguage] = useState('');

    const [timePassedOnExerciseInSeconds, setTimePassedOnExerciseInSeconds] = useState(0);
    const [timePassedOnExerciseString, setTimePassedOnExerciseString] = useState('XX:XX:XX');

    const [numberOfValidatedTests, setNumberOfValidatedTests] = useState(0);
    const [hasExecutedOnce, setHasExecutedOnce] = useState(false);

    const { challengeId } = useParams();

    const getExerciseAndChallengeInfo = async () => {
        const challenge = await ChallengeService.getChallengeDetails(challengeId);
        setChallengeInfo(challenge);

        if (getCurrentUserChallengeInfos(challenge, user.id).hasSubmittedExercise) {
            goToChallengeDetails(challengeId);
            return;
        }

        const exerciseRetrieved = await ChallengeService.getExercise(parseInt(challenge.exercise.id));
        setExercise(exerciseRetrieved);

        setCodeLanguage(exerciseRetrieved.language);
        // Get user code in challenge infos to override defaultCode
        const userCurrentChallengeInfos = getCurrentUserChallengeInfos(challenge, user.id);
        const defaultCode = userCurrentChallengeInfos?.userCode?.code ?? exerciseRetrieved.defaultCode ?? '';
        setCodeContent(defaultCode);
        setTimePassedOnExerciseInSeconds(userCurrentChallengeInfos.timeSpentOnExercise);
    };

    useEffect(() => {
        getExerciseAndChallengeInfo();
    }, []);

    useEffect(() => {
        if (!exercise || !challengeInfo) {
            return;
        }
        const timer = setInterval(() => {
            setTimePassedOnExerciseInSeconds(timePassedOnExerciseInSeconds + 1);
            setTimePassedOnExerciseString(DurationService.getFormattedSecondsInHourMinuteSeconds(timePassedOnExerciseInSeconds));

            // Update time passed on challenge with API
            ChallengeService.updateTimePassedOnChallenge(challengeId, user.id, timePassedOnExerciseInSeconds);
        }, 1000);
        return () => clearInterval(timer);
    });

    const retrieveCodeAndLanguage = (code, language) => {
        setCodeContent(code);
        setCodeLanguage(language);
    };


    const handleTabClose = event => {
        // If event comes from Submit code button then we don't want to close the tab
        if (event.target.activeElement?.type === 'submit') {
            return;
        }
        event.preventDefault();
        console.log('beforeunload event triggered');
        event.returnValue = 'Êtes-vous sûr de quitter cette page ? Le chronomètre sera toujours enregistré mais si le code n\'a pas été exécuté, il ne sera pas sauvegardé !';
        return event;
    };

    useEffect(() => {
        window.addEventListener('beforeunload', handleTabClose);

        return () => {
            window.removeEventListener('beforeunload', handleTabClose);
        };
    }, []);

    return (
        <>
            {(!exercise || !challengeInfo) && (
                <>
                    <Header
                        pageTitle={''}
                        leftButtonContent={'Mes snippets'}
                        leftButtonFunction={goToSnippets}
                    />
                    <CircularProgress className='CircularLoader' />
                </>
            )}
            {exercise && challengeInfo && (
                <>
                    <Header
                        pageTitle={''}
                        leftButtonContent={'Mes snippets'}
                        leftButtonFunction={goToSnippets}
                    />
                    <div className='SolveExercise'>
                        <Grid container direction={'column'} justifyContent={'center'} justifyItems={'center'}
                            alignContent={'center'} alignItems={'center'} justifySelf={'center'}>
                            <Typography variant='h3' component='h1' fontWeight={'bold'} sx={{ margin: 1 }}>
                                {exercise.name}
                            </Typography>
                            <Grid className='ExerciseInstructions'>
                                <Typography variant='body1' component='p'>
                                    {exercise.instructions}
                                </Typography>
                            </Grid>
                            <br />
                            <Grid className='ExerciseDifficultyAndLanguage'>
                                <Typography variant='subtitle1' component='h2' fontStyle={'italic'}>
                                    Difficulté : {exercise.difficulty} - {exercise.language}
                                </Typography>
                            </Grid>

                            {/* Timer when page is loaded to count resolution exercise time */}
                            <Grid className='ExerciseTimer' sx={{ margin: 1 }}>
                                <Typography variant='h5' component='h2' fontStyle={'italic'}>
                                    Temps passé sur l'exercice
                                    : {timePassedOnExerciseString}
                                </Typography>
                            </Grid>

                            {/* Button submit and forgive */}
                            <Grid>
                                <Button type='submit' className={'CapitalizedButton'} variant='contained'
                                    color='primary'
                                    disabled={!hasExecutedOnce}
                                    onClick={() => {
                                        const areYouSureToSubmit = confirm('Êtes-vous sûr de vouloir valider votre code ?');
                                        if (!areYouSureToSubmit) {
                                            return;
                                        }

                                        ChallengeService.submitExercise(exercise, user.id, challengeId, codeContent, codeLanguage, numberOfValidatedTests).then(() => {
                                            goToChallengeDetails(challengeId);
                                        });
                                    }}>
                                    Soumettre
                                </Button>
                            </Grid>
                        </Grid>
                        <ExerciseResolutionEditor userId={user.id} exercise={exercise} challengeId={challengeId}
                            parentCallback={retrieveCodeAndLanguage} defaultCode={codeContent}
                            defaultLanguage={codeLanguage} setHasExecutedOnce={setHasExecutedOnce}
                            setNumberOfValidatedTests={setNumberOfValidatedTests}></ExerciseResolutionEditor>
                    </div>
                </>
            )}
        </>
    );
}
