import { Button, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import React, { useState } from 'react';
import { goToSnippets } from '../../services/routing.service';
import UserService from '../../services/user.service';
import { ChallengeService, getOpponentChallengeInfos, getCurrentUserChallengeInfos } from '../collaborative/challenges/Challenge.service';
import { Header } from '../header/Header';
import './MesChallenges.css';

export default function MesChallenges() {
    const [challenges, setChallenges] = useState([]);

    return (
        <div className='Challenges'>
            <Grid container>
                <Header
                    pageTitle={'Historique des challenges'}
                    leftButtonContent={'Mes snippets'}
                    leftButtonFunction={goToSnippets}
                />

                <Button onClick={() => getUserChallengesIsChallenger(setChallenges)}>Challenge demandés</Button>
                <Button onClick={() => getUserChallengesIsChallenged(setChallenges)}>Challenge reçues</Button>
            </Grid>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Nom du challenge</TableCell>
                            <TableCell align="right">Difficulté</TableCell>
                            <TableCell align="right">Langage</TableCell>
                            <TableCell align="right">A validé l'exercice</TableCell>
                            <TableCell align="right">Score</TableCell>
                            <TableCell align="right">Adversaire</TableCell>
                            <TableCell align="right">A validé l'exercice</TableCell>
                            <TableCell align="right">Score</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {challenges.map((element) => (
                            <TableRow
                                key={"key"}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell align="right">{element.challenge.exercise.name}</TableCell>
                                <TableCell align="right">{element.challenge.exercise.difficulty}</TableCell>
                                <TableCell align="right">{element.challenge.exercise.language}</TableCell>
                                <TableCell align="right">{element.challenge.challengeInfos[0].hasValidatedExercise == true ? 'Oui' : 'Non'}</TableCell>
                                <TableCell align="right">{element.challenge.challengeInfos[0].score}</TableCell>
                                <TableCell align="right">{element.challenge.challengeInfos[1].user.pseudo}</TableCell>
                                <TableCell align="right">{element.challenge.challengeInfos[1].hasValidatedExercise == true ? 'Oui' : 'Non'}</TableCell>
                                <TableCell align="right">{element.challenge.challengeInfos[1].score}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>    
    );
}

async function getUserChallengesIsChallenger(setChallenges: any){
    const userId = UserService.getCurrentUser().id;
    const userChallenges = await ChallengeService.getUserChallengesIsChallenger(userId);
    let currentUser, opponent;
    userChallenges.forEach(element =>{
        currentUser = getCurrentUserChallengeInfos(element.challenge,userId);
        opponent = getOpponentChallengeInfos(element.challenge,userId);
        element.challenge.challengeInfos[0] = currentUser;
        element.challenge.challengeInfos[1] = opponent;
    })
    setChallenges(userChallenges);
}

async function getUserChallengesIsChallenged(setChallenges: any){
    const userId = UserService.getCurrentUser().id;
    const userChallenges = await ChallengeService.getUserChallengesIsChallenged(userId);
    userChallenges.forEach(element =>{
        const currentUser = getCurrentUserChallengeInfos(element.challenge,userId);
        const opponent = getOpponentChallengeInfos(element.challenge,userId);
        element.challenge.challengeInfos[0] = currentUser;
        element.challenge.challengeInfos[1] = opponent;
    })
    setChallenges(userChallenges);
}