import React, { useEffect, useState } from 'react';
import './Snippets.css';
import { Button, Card, CardActions, CardContent, CircularProgress, Grid, IconButton, Typography } from '@mui/material';
import { Header } from '../header/Header';
import { goToDashboard } from '../../services/routing.service';
import UserService from '../../services/user.service';
import AddIcon from '@mui/icons-material/Add';
import { UpdateSnippetDto } from './DTO/update-snippet.dto';
import { SnippetsService } from './SnippetsService';
import CodeIcon from '@mui/icons-material/Code';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import TextField from '@mui/material/TextField';
import Editor from '../Editor/Editor';
import DialogActions from '@mui/material/DialogActions';
import Dialog from '@mui/material/Dialog';

export function Snippets() {
    const user = UserService.getCurrentUser();
    const [userSnippets, setUserSnippets] = useState(null);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [editDialogOpen, setEditDialogOpen] = useState(false);
    const [codeDialogOpen, setCodeDialogOpen] = useState(false);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [codeContent, setCodeContent] = useState('');
    const [codeLanguage, setCodeLanguage] = useState('');

    const retrieveCodeAndLanguage = (code, language) => {
        setCodeContent(code);
        setCodeLanguage(language);
    };

    const retrieveUserSnippets = async () => {
        const userSnippets = await SnippetsService.getUserSnippets(user.userId);
        setUserSnippets(userSnippets);
    };

    const updateSnippet = async (snippetId: number, snippet: UpdateSnippetDto) => {
        await SnippetsService.updateSnippet(snippetId, snippet);
        retrieveUserSnippets();
    };

    const deleteSnippet = async (snippetId) => {
        await SnippetsService.deleteSnippet(snippetId);
        retrieveUserSnippets();
    };

    const handleClickOpen = () => {
        setDialogOpen(true);
    };

    const handleClose = () => {
        setDialogOpen(false);
    };

    const handleEditClickOpen = (snippet) => {
        setTitle(snippet.title);
        setDescription(snippet.description);
        setCodeContent(snippet.code.code);
        setCodeLanguage(snippet.code.language);
        setEditDialogOpen(true);
    };

    const handleCodeClickOpen = (snippet) => {
        setTitle(snippet.title);
        setDescription(snippet.description);
        setCodeContent(snippet.code.code);
        setCodeLanguage(snippet.code.language);
        setCodeDialogOpen(true);
    };

    const handleCodeClose = () => {
        setCodeDialogOpen(false);
    };

    const handleEditClose = () => {
        setEditDialogOpen(false);
    };

    const handleEditSubmit = async (snippet) => {
        console.log('Updating snippet with title ' + title + ' and description ' + description, ' and code ' + codeContent + ' and language ' + codeLanguage);
        const snippetToUpdate = {
            id: snippet.id,
            title,
            description,
            codeContent,
            codeLanguage,
        };
        updateSnippet(snippetToUpdate.id, snippetToUpdate).then(() => {
            setEditDialogOpen(false);
        });
    };

    const handleSubmit = async () => {
        await SnippetsService.createSnippet({
            title,
            description,
            userId: user.id,
            code: {
                language: codeLanguage,
                code: codeContent,
                title,
            },
        });
        setDialogOpen(false);
        window.location.reload();
    };

    useEffect(() => {
        retrieveUserSnippets();
    }, []);

    return (
        <div className='Snippets'>
            <Grid container>
                <Header
                    pageTitle={'Mes snippets'}
                    leftButtonFunction={goToDashboard}
                    leftButtonContent={'Accueil'}
                />
                {/*  Button create snippet  */}
                <Grid direction='column' container alignItems='center' justifyContent='end' justifyItems='center'
                    style={{ marginTop: 10 }} item xs={12} md={12} lg={12}>
                    <Button className={'CapitalizedButton'} variant='contained' onClick={handleClickOpen}
                        startIcon={<AddIcon />}
                        style={{ marginRight: 10 }}>
                        Créer un nouveau snippet
                    </Button>
                </Grid>
                <Dialog open={dialogOpen} onClose={handleClose} fullWidth={true} maxWidth={'lg'}>
                    <DialogTitle>Créer un snippet</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Pour créer un snippet, veuillez entrer le titre, le code et le langage du snippet.
                            Vous pouvez aussi ajouter une description.
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin='dense'
                            id='snippet-title'
                            label='Titre du snippet'
                            type='title'
                            defaultValue={title}
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                            fullWidth
                            variant='standard'
                        />
                        <TextField
                            autoFocus
                            margin='dense'
                            multiline
                            id='snippet-description'
                            label='Description du snippet'
                            type='description'
                            defaultValue={description}
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                            fullWidth
                            variant='standard'
                        />
                        <Editor isUsedInForm={true} ref={null} parentCallback={retrieveCodeAndLanguage}
                            defaultCode={codeContent} defaultLanguage={codeLanguage} />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Annuler</Button>
                        <Button onClick={handleSubmit}>Poster</Button>
                    </DialogActions>
                </Dialog>

                {!userSnippets && <CircularProgress className='CircularLoader' />}
                {userSnippets && userSnippets.length === 0 && (
                    <Typography variant='h6' style={{ marginTop: 20, margin: 'auto' }}>
                        Vous n'avez pas de snippets ! Essayez de créer un nouveau snippet ! Cela pourrait vous être
                        utile !
                    </Typography>
                )}
                {userSnippets && userSnippets.length > 0 && (
                    <Grid item xs={12} md={12} lg={12}>
                        {/*  List snippets with different button, details/edit/remove  */}
                        <div className='snippet-list'>
                            {userSnippets.map((snippet) => (
                                <Card sx={{ maxWidth: 345, margin: 5 }}>
                                    <CardContent>
                                        <Typography gutterBottom variant='h5' component='div'>
                                            {snippet.title}
                                        </Typography>
                                        <Typography variant='body2' color='text.secondary'>
                                            {snippet.description}
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <IconButton color={'info'}
                                            onClick={() => handleCodeClickOpen(snippet)}><CodeIcon /></IconButton>
                                        <Dialog open={codeDialogOpen} onClose={handleCodeClose} fullWidth={true}
                                            maxWidth={'lg'}>
                                            <DialogTitle>Mon snippet</DialogTitle>
                                            <DialogContent>
                                                <Editor isUsedInForm={true} ref={null}
                                                    parentCallback={retrieveCodeAndLanguage}
                                                    defaultCode={codeContent} />
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleCodeClose}>Fermer</Button>
                                            </DialogActions>
                                        </Dialog>
                                        <IconButton color={'success'}
                                            onClick={() => handleEditClickOpen(snippet)}><EditIcon /></IconButton>
                                        <Dialog open={editDialogOpen} onClose={handleEditClose} fullWidth={true}
                                            maxWidth={'lg'}>
                                            <DialogTitle>Modifier un snippet</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    Vous pouvez modifier le titre, la description et le code du snippet.
                                                </DialogContentText>
                                                <TextField
                                                    autoFocus
                                                    margin='dense'
                                                    id={'snippet-title-edit' + snippet.id}
                                                    label='Titre du snippet'
                                                    type='title'
                                                    defaultValue={title}
                                                    value={title}
                                                    onChange={(e) => setTitle(e.target.value)}
                                                    fullWidth
                                                    variant='standard'
                                                />
                                                <TextField
                                                    autoFocus
                                                    margin='dense'
                                                    multiline
                                                    id={'snippet-description-edit' + snippet.id}
                                                    label='Description du snippet'
                                                    type='description'
                                                    defaultValue={description}
                                                    value={description}
                                                    onChange={(e) => setDescription(e.target.value)}
                                                    fullWidth
                                                    variant='standard'
                                                />
                                                <Editor isUsedInForm={true} ref={null}
                                                    parentCallback={retrieveCodeAndLanguage}
                                                    defaultCode={codeContent} defaultLanguage={codeLanguage} />
                                                {/*codeFromParent={snippet.code} />*/}
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleEditClose}>Annuler</Button>
                                                <Button onClick={() => handleEditSubmit(snippet)}>Poster</Button>
                                            </DialogActions>
                                        </Dialog>
                                        <IconButton color={'error'}
                                            onClick={() => deleteSnippet(snippet.id)}><DeleteIcon /></IconButton>
                                    </CardActions>
                                </Card>
                            ))}
                        </div>
                    </Grid>
                )}
            </Grid>
        </div>
    );
}
