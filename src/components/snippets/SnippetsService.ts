import { ApiService } from '../../services/api.service';
import { UpdateSnippetDto } from './DTO/update-snippet.dto';
import { CreateSnippetDto } from './DTO/create-snippet.dto';

export class SnippetsService {
    static createSnippet(snippet: CreateSnippetDto) {
        const createSnippetUrl = `${process.env.REACT_APP_API_URL}/snippets`;
        return ApiService.post(createSnippetUrl, snippet);
    }

    static getUserSnippets(userId) {
        const getUserSnippetsUrl = `${process.env.REACT_APP_API_URL}/users/${userId}/snippets`;
        return ApiService.get(getUserSnippetsUrl);
    }

    static async updateSnippet(snippetId: number, snippet: UpdateSnippetDto) {
        const updateSnippetUrl = `${process.env.REACT_APP_API_URL}/snippets/${snippetId}`;
        return ApiService.patch(updateSnippetUrl, snippet);
    }

    static async deleteSnippet(snippetId) {
        const deleteSnippetUrl = `${process.env.REACT_APP_API_URL}/snippets/${snippetId}`;
        return ApiService.delete(deleteSnippetUrl);
    }
}