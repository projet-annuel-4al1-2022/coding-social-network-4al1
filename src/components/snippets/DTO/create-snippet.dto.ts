export class CreateSnippetDto {
    readonly title: string;
    readonly description: string;
    readonly code: {
        readonly title: string;
        readonly code: string;
        readonly language: string;
    };
    readonly userId: number;
}
