export class UpdateSnippetDto {
    readonly id: number;
    readonly title?: string;
    readonly description?: string;
    readonly code?: string;
    readonly language?: string;
}
