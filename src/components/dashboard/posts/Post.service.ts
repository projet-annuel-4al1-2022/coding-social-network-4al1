import { ApiService } from '../../../services/api.service';

export class PostService {

    static async getUserFeed(userId: number, date: Date) {
        const userFeedApiUrl = `${
            process.env.REACT_APP_API_URL
        }/users/${userId}/feed?maxDate=${date.toISOString()}&userRequestingId=${userId}`;
        return ApiService.get<[]>(userFeedApiUrl);
    }

    static upVote(postId: number, userId: number) {
        const upVoteApiUrl = `${process.env.REACT_APP_API_URL}/posts/${postId}/upVote/${userId}`;
        return ApiService.post(upVoteApiUrl);
    }

    static downVote(postId: number, userId: number) {
        const downVoteApiUrl = `${process.env.REACT_APP_API_URL}/posts/${postId}/downVote/${userId}`;
        return ApiService.post(downVoteApiUrl);
    }

    static getPostById(postId: string, userId: number) {
        const getPostByIdUrl = `${process.env.REACT_APP_API_URL}/posts/${postId}?userId=${userId}`;
        return ApiService.get(getPostByIdUrl);
    }

    static async postCommentWithCode(postId: number, commentContent: string, authorId: number, code: string, language: string) {
        const commentPostUrl = `${process.env.REACT_APP_API_URL}/comments`;
        console.log('Creating post with content: ', commentContent, ' and authorId: ', authorId, ' and postId: ', postId);
        return ApiService.post(commentPostUrl, {
            postId,
            content: commentContent,
            authorId,
            code: {
                'title': `Proposition de code de l'utilisateur '${authorId}' - pour le post : '${postId}' - à la date : ${new Date().toISOString()}`,
                'code': code,
                'language': language,
            },
        });
    }

    static async postCommentWithoutCode(postId: number, commentContent: string, authorId: number) {
        const commentPostUrl = `${process.env.REACT_APP_API_URL}/comments`;
        console.log('Creating post with content: ', commentContent, ' and authorId: ', authorId, ' and postId: ', postId);
        return ApiService.post(commentPostUrl, {
            postId,
            content: commentContent,
            authorId,
        });
    }

    static async getPostComments(postId: string, userId: number) {
        const getPostCommentsUrl = `${process.env.REACT_APP_API_URL}/posts/${postId}/comments?userId=${userId}`;
        return ApiService.get(getPostCommentsUrl);
    }

    static async getUserPosts(userId: number, userRequestingId: number) {
        const userPostsApiUrl = `${
            process.env.REACT_APP_API_URL
        }/users/${userId}/posts?userRequestingId=${userRequestingId}`;
        return ApiService.get<[]>(userPostsApiUrl);
    }

    static async searchPosts(query) {
        const searchPostsUrl = `${process.env.REACT_APP_API_URL}/posts/search?query=${query}`;
        return ApiService.get(searchPostsUrl);
    }
}
