import * as React from 'react';
import { useEffect, useState } from 'react';
import './Posts.css';
import { Button, ButtonBase, Card, CardActions, CardContent, CircularProgress, Grid, Typography } from '@mui/material';
import { User } from '../../../models/User.model';
import { Duration } from '../../../helpers/duration.helper';
import SyntaxHighlighter from 'react-syntax-highlighter';
import theme from 'react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark';
import { goToPostDetails } from '../../../services/routing.service';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { PostService } from './Post.service';
import CreatePostDialog from './createPost/CreatePost';
import { seeAuthor } from '../../userPages/postsCard/UserPosts';

export function Posts(user: User) {
    const [posts, setPosts] = useState(null);

    const getUserFeed = async (userId: number, date: Date) => {
        const postsResponse = await PostService.getUserFeed(userId, date);
        console.log(postsResponse);
        setPosts(postsResponse);
    };

    useEffect(() => {
        getUserFeed(user.userId, new Date());
    }, []);
    return (
        <div>
            <Grid direction='column' container alignItems='center' justifyContent='end' justifyItems='center'
                style={{ marginTop: 10 }}>
                <CreatePostDialog />
            </Grid>
            {!posts && <CircularProgress className='CircularLoader' />}
            {posts && posts.length === 0 && (
                <Typography className='NoPostFound' variant='h6' style={{ marginTop: 10 }}>
                    Pas de post trouvé. Essayez de suivre quelqu'un !
                </Typography>
            )}
            {posts && posts.length > 0 &&
                posts.map((post) => {
                    return (
                        <Card variant='outlined' className='PostContainer' key={post.id}>
                            <Grid container flexDirection={'row'}>
                                <Grid
                                    item
                                    xs={1}
                                    md={1}
                                    lg={1}
                                    flexDirection={'column'}
                                    alignItems={'center'}
                                    alignContent={'center'}
                                    alignSelf={'center'}
                                    justifyContent={'center'}
                                    justifyItems={'center'}
                                    paddingRight='10px'
                                >
                                    <Button
                                        className='ButtonWithoutBackgroundAndBorders'
                                        onClick={() => {
                                            if (!post.hasUserUpVoted) {
                                                voteOnPost(post, user, true);
                                            }
                                        }}
                                    >
                                        <KeyboardArrowUpIcon />
                                    </Button>
                                    <Typography
                                        id={'post' + post.id + 'score'}
                                        variant='h6'
                                        sx={{ minWidth: '64px' }}
                                        color={
                                            post.hasUserUpVoted
                                                ? 'green'
                                                : post.hasUserDownVoted
                                                    ? 'red'
                                                    : 'black'
                                        }
                                    >
                                        {post.score}
                                    </Typography>
                                    <Button
                                        className='ButtonWithoutBackgroundAndBorders'
                                        onClick={() => {
                                            if (!post.hasUserDownVoted) {
                                                voteOnPost(post, user, false);
                                            }
                                        }}
                                    >
                                        <KeyboardArrowDownIcon />
                                    </Button>
                                </Grid>
                                <Grid item xs={11} md={11} lg={11}>
                                    <Button
                                        size='large'
                                        sx={{
                                            fontSize: 14,
                                            color: 'text.secondary',
                                        }}
                                        onClick={() => {
                                            seeAuthor(
                                                post.author.id,
                                            );
                                        }}
                                        style={{ margin: 'auto' }}
                                    >
                                        @
                                        {post?.author.pseudo ??
                                            `Post's author`}
                                        {' - '}
                                        {Duration.timeSinceFrench(
                                            post.createdAt,
                                        )}
                                    </Button>
                                    <ButtonBase
                                        className='ClickableCard'
                                        onClick={() => goToPostDetails(post.id)}
                                    >
                                        <React.Fragment>
                                            <CardContent className='PostContent'>
                                                <Typography
                                                    variant='h5'
                                                    color='text.secondary'
                                                    gutterBottom
                                                >
                                                    {post.title}
                                                </Typography>
                                                <Typography component='div' align='left'>
                                                    {post.content}
                                                </Typography>
                                                <br />
                                                {post.codes.length > 0 &&
                                                    post.codes.map((code) => {
                                                        return (
                                                            <>
                                                                <SyntaxHighlighter
                                                                    style={theme}
                                                                    className='HighlightedLanguage'
                                                                >
                                                                    {
                                                                        `/** Langage utilisé : ${code.language} **/`
                                                                    }
                                                                </SyntaxHighlighter>
                                                                <SyntaxHighlighter
                                                                    language={
                                                                        code.langage
                                                                    }
                                                                    style={theme}
                                                                    className='HighlightedCode'
                                                                >
                                                                    {code.code}
                                                                </SyntaxHighlighter>
                                                            </>
                                                        );
                                                    })}
                                            </CardContent>
                                        </React.Fragment>
                                    </ButtonBase>
                                    <CardActions>
                                        <Button
                                            size='large'
                                            onClick={() =>
                                                openSandbox(post.codes[0].code, post.codes[0].language)
                                            }
                                            style={{ margin: 'auto' }}
                                        >
                                            Ouvrir une sandbox {'< />'}
                                        </Button>
                                    </CardActions>
                                    <CardContent className='PostContent'>
                                        <Typography component='div' align='center'>
                                            {post.countComments || 0} commentaires
                                        </Typography>
                                    </CardContent>
                                </Grid>
                            </Grid>
                        </Card>
                    );
                })}
        </div>
    );
}

export const voteOnPost = (post, user, isUpVote: boolean) => {
    if (isUpVote) {
        upVote(post, user);
    } else {
        downVote(post, user);
    }
};

export function getScoreColor(post) {
    if (!post.hasUserDownVoted && !post.hasUserUpVoted) {
        return 'black';
    }
    if (post.hasUserUpVoted && !post.hasUserDownVoted) {
        return 'green';
    }
    if (!post.hasUserUpVoted && post.hasUserDownVoted) {
        return 'red';
    }
    if (post.hasUserUpVoted && post.hasUserDownVoted) {
        console.error('WEIRD - post.hasUserUpVoted && post.hasUserDownVoted is true');
    }
}

function upVote(post, user) {
    if (post.hasUserUpVoted) {
        return;
    }
    const postScore = document.getElementById('post' + post.id + 'score');
    post.score = parseInt(post.score) + 1;
    if (post.hasUserDownVoted) {
        post.hasUserDownVoted = false;
    } else {
        post.hasUserUpVoted = true;
    }
    postScore.innerHTML = String(post.score);
    postScore.style.color = getScoreColor(post);
    PostService.upVote(post.id, user.userId).then((r: unknown) => {
        console.log('Response upVote', r);
    });
}

function downVote(post, user) {
    if (post.hasUserDownVoted) {
        return;
    }
    const postScore = document.getElementById('post' + post.id + 'score');
    post.score = parseInt(post.score) - 1;
    if (post.hasUserUpVoted) {
        post.hasUserUpVoted = false;
    } else {
        post.hasUserDownVoted = true;
    }
    postScore.innerHTML = String(post.score);
    postScore.style.color = getScoreColor(post);
    PostService.downVote(post.id, user.userId).then((r: unknown) => {
        console.log('Response downVote', r);
    });
}

export const openSandbox = (code: string, language: string) => {
    localStorage.setItem('selectedLanguage', language);
    localStorage.setItem('code', code);
    window.open('/editor', '_blank');
};