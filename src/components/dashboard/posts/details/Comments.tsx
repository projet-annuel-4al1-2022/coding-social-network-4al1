import React, { useEffect, useState } from 'react';
import { Button, Card, CardContent, CircularProgress, Grid, Typography } from '@mui/material';
import { User } from '../../../../models/User.model';
import UserService from '../../../../services/user.service';
import { useParams } from 'react-router-dom';
import { PostService } from '../Post.service';
import { Duration } from '../../../../helpers/duration.helper';
import { getScoreColor } from '../Posts';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { CommentsService } from './Comments.service';
import { seeAuthor } from '../../../userPages/postsCard/UserPosts';
import theme from 'react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark';
import SyntaxHighlighter from 'react-syntax-highlighter';

export function Comments() {
    const user: User = UserService.getCurrentUser();

    const { postId } = useParams();
    const [comments, setComments] = useState(null);

    useEffect(() => {
        PostService.getPostComments(postId, user.userId).then((getCommentsHttpRequestResult: any) => {
            console.log('Got comments for post: ', getCommentsHttpRequestResult);
            setComments(getCommentsHttpRequestResult.comments);
        });
    }, []);

    return (
        <div>
            {!comments && <CircularProgress className='CircularLoader' />}
            {comments && comments.length === 0 && (
                <Typography className='NoPostFound' variant='h6' style={{ marginTop: 10 }}>
                    Pas de commentaire trouvé. Postez le premier commentaire de ce post !
                </Typography>
            )}
            {comments && comments.length > 0 &&
                comments.map((comment) => {
                    return (
                        <Card variant='outlined' className='PostContainer'>
                            <Grid container flexDirection={'row'}>
                                <Grid container flexDirection={'row'}>
                                    <Grid
                                        height='115%'
                                        container
                                        xs={1}
                                        md={1}
                                        lg={1}
                                        flexDirection={'column'}
                                        alignItems={'center'}
                                        justifyContent={'center'}
                                        alignContent={'center'}
                                        justifyItems={'center'}
                                        paddingRight='10px'
                                    >
                                        <Button
                                            className='ButtonWithoutBackgroundAndBorders'
                                            onClick={() => {
                                                if (!comment.hasUserUpVoted) {
                                                    upVoteComment(comment, user);
                                                }
                                            }}
                                        >
                                            <KeyboardArrowUpIcon />
                                        </Button>
                                        <Typography
                                            id={'comment' + comment.id + 'score'}
                                            variant='h6'
                                            color={
                                                comment.hasUserUpVoted
                                                    ? 'green'
                                                    : comment.hasUserDownVoted
                                                        ? 'red'
                                                        : 'black'
                                            }
                                        >
                                            {comment.score}
                                        </Typography>
                                        <Button
                                            className='ButtonWithoutBackgroundAndBorders'
                                            onClick={() => {
                                                if (!comment.hasUserDownVoted) {
                                                    downVoteComment(comment, user);
                                                }
                                            }}
                                        >
                                            <KeyboardArrowDownIcon />
                                        </Button>
                                    </Grid>
                                    <Grid xs={11} md={11} lg={11}>
                                        <React.Fragment>
                                            <Button
                                                size='large'
                                                sx={{
                                                    fontSize: 14,
                                                    color: 'text.secondary',
                                                }}
                                                onClick={() =>
                                                    seeAuthor(
                                                        comment.author.id,
                                                    )
                                                }
                                                style={{ margin: 'auto' }}
                                            >
                                                @
                                                {comment.author?.pseudo ??
                                                    `Post's author`}
                                                {' - '}
                                                {Duration.timeSinceFrench(
                                                    comment.createdAt,
                                                )}
                                            </Button>
                                            <CardContent className='PostContent'>
                                                <Typography component='div' align='left'>
                                                    {comment.content}
                                                </Typography>
                                                {comment.codeProposition && (
                                                    <>
                                                        <SyntaxHighlighter
                                                            style={theme}
                                                            className='HighlightedLanguage'
                                                        >
                                                            {
                                                                `/** Langage utilisé : ${comment.codeProposition?.language} **/\n\n`
                                                            }
                                                        </SyntaxHighlighter>
                                                        <SyntaxHighlighter
                                                            language={
                                                                comment.codeProposition?.language
                                                            }
                                                            style={theme}
                                                            className='HighlightedCode'
                                                        >
                                                            {
                                                                comment.codeProposition?.code
                                                            }
                                                        </SyntaxHighlighter>
                                                    </>
                                                )}
                                            </CardContent>
                                        </React.Fragment>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Card>
                    );
                })}
        </div>
    );
}

function upVoteComment(comment, user) {
    if (comment.hasUserUpVoted) {
        return;
    }
    const commentScore = document.getElementById('comment' + comment.id + 'score');
    comment.score = parseInt(comment.score) + 1;
    if (comment.hasUserDownVoted) {
        comment.hasUserDownVoted = false;
    } else {
        comment.hasUserUpVoted = true;
    }
    commentScore.innerHTML = String(comment.score);
    commentScore.style.color = getScoreColor(comment);
    CommentsService.upVote(comment.id, user.userId);
}

function downVoteComment(comment, user) {
    if (comment.hasUserDownVoted) {
        return;
    }
    const commentScore = document.getElementById('comment' + comment.id + 'score');
    comment.score = parseInt(comment.score) - 1;
    if (comment.hasUserUpVoted) {
        comment.hasUserUpVoted = false;
    } else {
        comment.hasUserDownVoted = true;
    }
    commentScore.innerHTML = String(comment.score);
    commentScore.style.color = getScoreColor(comment);
    CommentsService.downVote(comment.id, user.userId);
}
