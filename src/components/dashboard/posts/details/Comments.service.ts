import { ApiService } from '../../../../services/api.service';

export class CommentsService {
    static upVote(commentId: number, userId: number) {
        const upVoteApiUrl = `${process.env.REACT_APP_API_URL}/comments/${commentId}/upVote/${userId}`;
        return ApiService.post(upVoteApiUrl);
    }

    static downVote(commentId: number, userId: number) {
        const downVoteApiUrl = `${process.env.REACT_APP_API_URL}/comments/${commentId}/downVote/${userId}`;
        return ApiService.post(downVoteApiUrl);
    }
}