// React
import React, { useEffect, useState } from 'react';
import { Button, Card, CardActions, CardContent, CircularProgress, Grid, TextField, Typography } from '@mui/material';
import './Details.css';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { Duration } from '../../../../helpers/duration.helper';
import theme from 'react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark';
import { openSandbox, voteOnPost } from '../Posts';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { User } from '../../../../models/User.model';
import UserService from '../../../../services/user.service';
import { useParams } from 'react-router-dom';
import { PostService } from '../Post.service';
import { Comments } from './Comments';

import { seeAuthor } from '../../../userPages/postsCard/UserPosts';
import Editor from '../../../Editor/Editor';

export function Details() {
    const user: User = UserService.getCurrentUser();

    const { postId } = useParams();
    const [post, setPost] = useState(null);
    const [comment, setComment] = useState(null);

    const [codeContent, setCodeContent] = useState('');
    const [codeLanguage, setCodeLanguage] = useState('');

    const [hideCodeComment, setHideCodeComment] = useState(true);

    const getPost = async () => {
        const postHttpRequestResult = await PostService.getPostById(postId, user.userId);
        console.log('Retrieved post ', postHttpRequestResult);
        setPost(postHttpRequestResult);
    };

    useEffect(() => {
        getPost();
    }, []);

    const makeAddCodePossible = () => {
        setHideCodeComment(!hideCodeComment);
    };

    const retrieveCodeAndLanguage = (code, language) => {
        setCodeContent(code);
        setCodeLanguage(language);
    };

    const postComment = async () => {
        try {
            console.log('Posting comment with code ', codeContent, ' and language ', codeLanguage);
            if (hideCodeComment) {
                if (comment == null || comment === '') {
                    alert('Le contenu de votre commentaire ne peut pas être vide');
                    return;
                }
                await PostService.postCommentWithoutCode(Number(postId), comment, user.userId);
            } else {
                if (codeContent === '') {
                    alert('Le code ne peut pas être vide');
                    return;
                }
                if (codeLanguage === '') {
                    alert('Vous devez sélectionner un langage');
                    return;
                }
                await PostService.postCommentWithCode(Number(postId), comment, user.userId, codeContent, codeLanguage);
            }
            window.location.reload();
        } catch (error) {
            console.log('error');
            console.log(error);
        }
        await getPost();
    };

    return (
        <Card variant='outlined' className='PostContainer'>
            {!post && <CircularProgress className='CircularLoader' />}
            {post && (
                <Grid container flexDirection={'row'}>
                    <Grid container flexDirection={'row'}>
                        <Grid
                            height='250px'
                            container
                            xs={1}
                            md={1}
                            lg={1}
                            flexDirection={'column'}
                            alignItems={'center'}
                            justifyContent={'center'}
                            alignContent={'center'}
                            justifyItems={'center'}
                            paddingRight='10px'
                        >
                            <Button
                                className='ButtonWithoutBackgroundAndBorders'
                                onClick={() => {
                                    if (!post.hasUserUpVoted) {
                                        voteOnPost(post, user, true);
                                    }
                                }}
                            >
                                <KeyboardArrowUpIcon />
                            </Button>
                            <Typography
                                id={'post' + post.id + 'score'}
                                variant='h6'
                                color={
                                    post.hasUserUpVoted
                                        ? 'green'
                                        : post.hasUserDownVoted
                                            ? 'red'
                                            : 'black'
                                }
                            >
                                {post.score}
                            </Typography>
                            <Button
                                className='ButtonWithoutBackgroundAndBorders'
                                onClick={() => {
                                    if (!post.hasUserDownVoted) {
                                        voteOnPost(post, user, false);
                                    }
                                }}
                            >
                                <KeyboardArrowDownIcon />
                            </Button>
                        </Grid>
                        <Grid xs={11} md={11} lg={11}>
                            <React.Fragment>
                                <Button
                                    size='large'
                                    sx={{
                                        fontSize: 14,
                                        color: 'text.secondary',
                                    }}
                                    onClick={() => {
                                        seeAuthor(
                                            post.author.id,
                                        );
                                    }}
                                    style={{ margin: 'auto' }}
                                >
                                    @
                                    {post?.author?.pseudo ??
                                        `Post's author`}
                                    {' - '}
                                    {Duration.timeSinceFrench(
                                        post.createdAt,
                                    )}
                                </Button>
                                <CardContent className='PostContent'>
                                    <Typography
                                        variant='h5'
                                        color='text.secondary'
                                        gutterBottom
                                    >
                                        {post.title}
                                    </Typography>
                                    <Typography component='div' align='left'>
                                        {post.content}
                                    </Typography>
                                    <br />
                                    {post?.codes?.length > 0 &&
                                        post.codes.map((code) => {
                                            return (
                                                <>
                                                    <SyntaxHighlighter
                                                        style={theme}
                                                        className='HighlightedLanguage'
                                                    >
                                                        {
                                                            `/** Langage utilisé : ${code.language} **/`
                                                        }
                                                    </SyntaxHighlighter>
                                                    <SyntaxHighlighter
                                                        language={
                                                            code.language
                                                        }
                                                        style={theme}
                                                        className='HighlightedCode'
                                                    >
                                                        {
                                                            code.code
                                                        }
                                                    </SyntaxHighlighter>
                                                </>
                                            );
                                        })}
                                </CardContent>
                                <CardActions>
                                    <Button
                                        size='large'
                                        onClick={() => openSandbox(post.codes[0].code, post.codes[0].language)}
                                        style={{ margin: 'auto' }}
                                    >
                                        Ouvrir une sandbox {'< />'}
                                    </Button>
                                </CardActions>
                                <CardContent className='PostComments'>
                                    <Typography component='div' align='left'>
                                        {post.countComments || 0} commentaires
                                    </Typography>
                                    <br />
                                    <TextField
                                        id='comment-content'
                                        label='Poster un commentaire'
                                        multiline
                                        rows={4}
                                        variant='outlined'
                                        fullWidth
                                        value={comment}
                                        onChange={event => setComment(event.target.value)}
                                        error={comment === ''}
                                        helperText={comment === '' ? 'Empty field!' : ' '}
                                    />
                                    <Grid id={'comment-code'} hidden={hideCodeComment}>
                                        <Editor isUsedInForm={true} defaultCode={codeContent}
                                            defaultLanguage={codeLanguage}
                                            parentCallback={retrieveCodeAndLanguage} />
                                    </Grid>
                                    <Button variant='outlined' style={{ margin: 3 }} color={'secondary'}
                                        onClick={() => makeAddCodePossible()}>
                                        {hideCodeComment ? 'Ajouter un code' : 'Retirer'}
                                    </Button>
                                    <Button variant='contained' style={{ margin: 3 }}
                                        onClick={() => postComment()}>Poster</Button>
                                    <Comments />
                                </CardContent>
                            </React.Fragment>
                        </Grid>
                    </Grid>
                </Grid>
            )}
        </Card>
    );
}