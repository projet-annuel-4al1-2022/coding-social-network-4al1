import React from 'react';
import './../../Dashboard.css';
import { Grid } from '@mui/material';
import { Header } from '../../../header/Header';
import { goToSnippets } from '../../../../services/routing.service';
import { Trends } from '../../trends/Trends';
import { Followings } from '../../followings/Followings';
import { User } from '../../../../models/User.model';
import UserService from '../../../../services/user.service';
import { Details } from './Details';

// React component to see Post details, ability to comments, vote upvote
export class PostDetails extends React.Component {
    user: User;

    constructor(props: any) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
        };
        this.user = UserService.getCurrentUser();
    }

    render() {
        return (
            <div className='Dashboard'>
                <Grid container>
                    <Header
                        pageTitle={'Détails d\'un post'}
                        leftButtonContent={'Mes snippets'}
                        leftButtonFunction={goToSnippets}
                    />

                    <Grid
                        item
                        xs={2}
                        md={2}
                        lg={2}
                        borderRight='1px solid lightgrey'
                    >
                        <header className='TrendsSection'>
                            <h3>Tendances</h3>
                            <Trends
                                userId={this.user.userId}
                                email={this.user.email}
                                pseudo={this.user.pseudo}
                                firstName={this.user.firstName}
                                fullName={this.user.fullName}
                                lastName={this.user.lastName}
                            />
                        </header>
                    </Grid>

                    <Grid item xs={8} md={8} lg={8}>
                        <header className='FeedSection'>
                            <Details />
                        </header>
                    </Grid>

                    <Grid
                        item
                        xs={2}
                        md={2}
                        lg={2}
                        borderLeft='1px solid lightgrey'
                    >
                        <header className='FriendsSection'>
                            <h3>Abonnements</h3>
                            <Followings
                                userId={this.user.userId}
                                email={this.user.email}
                                pseudo={this.user.pseudo}
                                firstName={this.user.firstName}
                                fullName={this.user.fullName}
                                lastName={this.user.lastName}

                            />
                        </header>
                    </Grid>
                </Grid>
            </div>
        );
    }

}