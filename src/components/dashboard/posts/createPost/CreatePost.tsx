import * as React from 'react';
import { useRef, useState } from 'react';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import TextField from '@mui/material/TextField';
import DialogActions from '@mui/material/DialogActions';
import { Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import Editor from '../../../Editor/Editor';
import CreatePostService from './CreatePost.service';
import UserService from '../../../../services/user.service';

export default function CreatePostDialog() {
    const [dialogOpen, setDialogOpen] = useState(false);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [codeContent, setCodeContent] = useState('');
    const [codeLanguage, setCodeLanguage] = useState('');

    const user = UserService.getCurrentUser();

    const codeEditor = useRef(null);

    const handleClickOpen = () => {
        setDialogOpen(true);
    };

    const handleSubmit = async () => {
        // Call api to create a post
        await CreatePostService.createPost({
            title,
            content: description,
            authorId: user.id,
            codes: [
                {
                    language: codeLanguage,
                    code: codeContent,
                    title: 'Hello World',
                },
            ],
        });
        setDialogOpen(false);
    };

    const handleClose = () => {
        setDialogOpen(false);
    };

    const retrieveCodeAndLanguage = (code, language) => {
        setCodeContent(code);
        setCodeLanguage(language);
    };

    return (
        <>
            <Button className={'CapitalizedButton'} variant='contained' onClick={handleClickOpen}
                startIcon={<AddIcon />} style={{ marginRight: 10 }}>
                Créer un nouveau post
            </Button>
            <Dialog open={dialogOpen} onClose={handleClose} fullWidth={true} maxWidth={'lg'}>
                <DialogTitle>Créer un post</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Pour créer un post, veuillez entrer le titre et le contenu du post.
                        Vous pouvez aussi ajouter un ou plusieurs morceaux de code.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin='dense'
                        id='post-title'
                        label='Titre du post'
                        type='title'
                        defaultValue={title}
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                        fullWidth
                        variant='standard'
                    />
                    <TextField
                        autoFocus
                        margin='dense'
                        multiline
                        id='post-description'
                        label='Description du post'
                        type='description'
                        defaultValue={description}
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        fullWidth
                        variant='standard'
                    />
                </DialogContent>
                <Editor isUsedInForm={true} ref={codeEditor} parentCallback={retrieveCodeAndLanguage}
                    defaultLanguage={''} defaultCode={''} />
                <DialogActions>
                    <Button onClick={handleClose}>Annuler</Button>
                    <Button onClick={handleSubmit}>Poster</Button>
                </DialogActions>
            </Dialog>
        </>
    );
}