import { ApiService } from '../../../../services/api.service';
import { CreatePostDTO } from '../../../../models/dto/CreatePost.dto';

export default class CreatePostService {
    static createPost(post: CreatePostDTO) {
        const createPostApiUrl = `${
            process.env.REACT_APP_API_URL
        }/posts`;
        return ApiService.post(createPostApiUrl, post);
    }
}