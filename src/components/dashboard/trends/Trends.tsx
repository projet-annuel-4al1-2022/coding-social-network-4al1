import React, { useEffect, useState } from 'react';
import './Trends.css';
import { Button, Card, Typography } from '@mui/material';
import { User } from '../../../models/User.model';
import { goToPostDetails } from '../../../services/routing.service';

export function Trends(user: User) {
    const [trends, setTrends] = useState([]);

    useEffect(() => {
        getUserTrends(user.userId).then(userTrendsResponse => {
            console.log('Got user trends: ', userTrendsResponse);
            setTrends(userTrendsResponse);
        });

    }, []);

    const getUserTrends = async (userId: number) => {
        const userTrendsApiUrl = `${process.env.REACT_APP_API_URL}/users/${userId}/trends`;

        const response = await fetch(userTrendsApiUrl);
        if (200 < response['status'] && response['status'] > 299) {
            throw new Error('Failed retrieving user\'s trends');
        }
        return await response.json();
    };

    return (
        <div>
            {trends.length === 0 && (
                <Typography className='NoTrendsFound' variant='h6'>
                    Il n'y a pas de posts récents.
                </Typography>
            )}
            {trends.length > 0 &&
                trends.map((trend) => {
                    return (
                        <Card variant='outlined' className='TrendContainer' key={trend.id}>
                            <Button
                                size='large'
                                sx={{
                                    fontSize: 14,
                                    color: 'text.primary',
                                    fontWeight: 'bold',
                                }}
                                onClick={() => goToPostDetails(trend.id)}
                                style={{ margin: 'auto' }}
                                className='TrendClickable'
                            >
                                @{trend?.author.pseudo}
                                <React.Fragment>
                                    <Typography
                                        className='NbOfPosts'
                                        sx={{
                                            fontSize: 12,
                                            color: 'text.secondary',
                                        }}
                                    >
                                        {trend?.title}
                                    </Typography>
                                </React.Fragment>
                            </Button>
                        </Card>
                    );
                })}
        </div>
    );
}
