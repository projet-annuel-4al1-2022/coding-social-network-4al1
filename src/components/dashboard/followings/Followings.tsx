import React, { useEffect, useState } from 'react';
import './Followings.css';
import { Button, Card, Typography } from '@mui/material';
import { User } from '../../../models/User.model';
import { seeAuthor } from '../../userPages/postsCard/UserPosts';

export function Followings(user: User) {
    const [followings, setFollowings] = useState([]);

    useEffect(() => {
        getUserFollowings(user.userId).then(userFollowingsResponse => {
            console.log('Got user followings: ', userFollowingsResponse);
            setFollowings(userFollowingsResponse);
        });

    }, []);

    const getUserFollowings = async (userId: number) => {
        const userFollowingsApiUrl = `${process.env.REACT_APP_API_URL}/users/${userId}/following`;

        const response = await fetch(userFollowingsApiUrl);
        if (200 < response['status'] && response['status'] > 299) {
            throw new Error('Failed retrieving user\'s following');
        }
        return response.json();
    };

    return (
        <div>
            {followings.length === 0 && (
                <Typography className='NoFollowingsFound' variant='h6'>
                    Vous ne suivez personne.
                </Typography>
            )}
            {followings.length > 0 &&
                followings.map((following) => {
                    return (
                        <Card
                            variant='outlined'
                            className='FollowingsContainer'
                            key={following.id}
                        >
                            <Button
                                size='large'
                                sx={{
                                    fontSize: 14,
                                    color: 'text.primary',
                                    fontWeight: 'bold',
                                    flexDirection: 'column',
                                    width: '100%',
                                }}
                                onClick={() => seeAuthor(following?.id)}
                                style={{ margin: 'auto' }}
                            >
                                @{following?.pseudo}
                                <React.Fragment>
                                    <Typography
                                        className='NbOfPosts'
                                        sx={{
                                            fontSize: 12,
                                            color: 'text.secondary',
                                        }}
                                    >
                                        {following?.postsCount} posts
                                    </Typography>
                                </React.Fragment>
                            </Button>
                        </Card>
                    );
                })}
        </div>
    );
}
