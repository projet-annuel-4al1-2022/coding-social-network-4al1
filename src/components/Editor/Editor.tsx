import './Editor.css';
import React, { useEffect, useImperativeHandle, useState } from 'react';
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-java';
import 'ace-builds/src-noconflict/mode-c_cpp';
import 'ace-builds/src-noconflict/mode-python';
import 'ace-builds/src-noconflict/mode-csharp';
import 'ace-builds/src-noconflict/theme-monokai';
import axios from 'axios';
import { Button, CircularProgress } from '@mui/material';
import { codeExecutorAPIUrl, findLanguageByName, languagesHighlightRules } from '../../services/code-executor.service';

const selectedLanguage = localStorage.getItem('selectedLanguage');
const idLanguage = localStorage.getItem('idLanguage');
const code = localStorage.getItem('code');

if (idLanguage === null) {
    languagesHighlightRules.every(element => {
        if (element.value === selectedLanguage) {
            localStorage.setItem('idLanguage', element.id.toString());
            return false;
        }
    });
}

function Editor({
    isSandbox = false,
    isUsedInForm = false,
    ref = null,
    parentCallback = null,
    codeFromParent = null,
    defaultCode = null,
    defaultLanguage = null,
}) {
    // Confirm user wants to quit on unload
    let sourceCode = codeFromParent?.code || (defaultCode == null ? code : defaultCode);
    let sourceLanguage = codeFromParent?.language || (defaultLanguage == null ? selectedLanguage : defaultLanguage);

    // const [sourceCode, setSourceCode] = useState(codeFromParent?.code ?? (defaultCode == null ? code : defaultCode));
    // const [sourceLanguage, setSourceLanguage] = useState(codeFromParent?.language ?? (defaultLanguage == null ? '' : defaultLanguage));
    const [sourceLanguageId, setSourceLanguageId] = useState(findLanguageByName(sourceLanguage) != null ? findLanguageByName(sourceLanguage).id : idLanguage);
    const [output, setOutput] = useState('');
    const [isExecuting, setIsExecuting] = useState(false);

    useImperativeHandle(ref, () => ({
        getMyState: () => {
            return { sourceCode };
        },
    }), [{ sourceCode }]);

    function changeLanguage(e, isUsedInForm) {
        const selectedID = e.target.value;
        console.log(selectedID);
        localStorage.setItem('idLanguage', selectedID);
        for (let i = 0; i < languagesHighlightRules.length; i++) {
            const language = languagesHighlightRules[i];
            if (language.id == selectedID) {
                console.log(language.value);
                sourceLanguage = language.value;
                setSourceLanguageId(language.id);
                localStorage.setItem('selectedLanguage', language.value);
                console.log(language.id + '\n' + language.value);
                console.log('Updated source language name = ', sourceLanguage);
                console.log('Updated source language id = ', sourceLanguageId);
                parentCallback(sourceCode, sourceLanguage);
                break;
            }
        }
        if (!isUsedInForm) {
            window.location.reload();
        }
    }

    function submitCode(sourceCode, idLanguage, setOutput) {
        setIsExecuting(true);
        setOutput('');
        const data = JSON.stringify({
            'source_code': sourceCode,
            'language_id': idLanguage,
            //"redirect_stderr_to_stdout": true,
        });

        const config = {
            method: 'post',
            url: codeExecutorAPIUrl + '/submissions',
            headers: {
                'content-type': 'application/json',
                'Content-Type': 'application/json',
                'X-RapidAPI-Key': '5e848af460msh11bc3ec13fe10eap1908c6jsn637af75facc5',
                'X-RapidAPI-Host': 'judge0-ce.p.rapidapi.com'
            },
            data: data,
        };

        console.log(sourceCode + '\n' + idLanguage);
        axios(config)
            .then(function(response) {
                let token = JSON.stringify(response.data.token);
                token = token.split('"')[1];
                setTimeout(getOutputCode, 3000, token, setOutput);
                //setTimeout(console.log,7000,result);
            })
            .catch(function(error) {
                console.log(error);
                setIsExecuting(false);
                alert('Veuillez préciser un code et un language valide !');
            });
    }

    function getOutputCode(token, setOutput) {
        const apiURL = codeExecutorAPIUrl + '/submissions/' + token;
        //console.log(apiURL);

        const config = {
            method: 'get',
            url: apiURL,
            headers: {
                'X-RapidAPI-Key': '5e848af460msh11bc3ec13fe10eap1908c6jsn637af75facc5',
                'X-RapidAPI-Host': 'judge0-ce.p.rapidapi.com'
            },
        };

        axios(config)
            .then(function(response) {
                const stdout = JSON.stringify(response.data.stdout);
                let result;
                if (stdout === 'null') {
                    result = JSON.stringify(response.data.stderr);
                } else {
                    result = stdout;
                }
                result = JSON.parse(result);
                //result = JSON.parse(result);
                setOutput(result);
                setIsExecuting(false);
            })
            .catch(function(error) {
                setOutput(error.message);
                setIsExecuting(false);
            });
    }

    if (isSandbox) {
        const handleTabClose = event => {
            event.preventDefault();
            event.returnValue = 'Êtes-vous sûr de quitter cette page ?';
            console.log('When tab is closed');
            localStorage.setItem('idLanguage', sourceLanguageId.toString());
            console.log('Saving language id = ', sourceLanguageId);
            localStorage.setItem('selectedLanguage', sourceLanguage);
            console.log('Saving language name = ', sourceLanguage);
            localStorage.setItem('code', sourceCode);
            console.log('Saving code = ', sourceCode);
            return event;
        };
        useEffect(() => {
            window.addEventListener('beforeunload', handleTabClose);

            return () => {
                window.removeEventListener('beforeunload', handleTabClose);
            };
        }, []);
    }

    return (
        <div className='App'>
            <head>
                <title>Codeboard Online IDE</title>
                <link rel='stylesheet' href='css/style.css' />
            </head>
            <body>
                <div className='header EditorHeader'>Compilateur en ligne</div>
                <div className='control-panel'>
                Sélectionner le langage
                &nbsp; &nbsp;
                    <select id='languages' className='languages'
                        defaultValue={sourceLanguageId}
                        onChange={(e) => changeLanguage(e, isUsedInForm)}
                    >
                        {languagesHighlightRules.map((language) => (
                            <option value={language.id}>{language.label}</option>
                        ))}
                    </select>
                </div>

                <div className='editor' id='editor'>
                    <AceEditor
                        height='100%'
                        width='100%'
                        mode={selectedLanguage}
                        theme='monokai'
                        name='aceEditor'
                        onChange={(codeContent) => {
                            sourceCode = codeContent;
                            if (parentCallback) {
                                parentCallback(codeContent, sourceLanguage);
                                console.log('Code updated with language = ', sourceLanguage);
                            }
                        }}
                        value={sourceCode}
                        fontSize={14}
                        showPrintMargin={true}
                        showGutter={true}
                        highlightActiveLine={true}
                        setOptions={{
                            enableBasicAutocompletion: false,
                            enableLiveAutocompletion: false,
                            enableSnippets: false,
                            showLineNumbers: true,
                            tabSize: 4,
                            readOnly: false,
                        }}
                    />
                </div>
                <div className='button-container'>
                    <Button variant='contained' className='CapitalizedButton' color='success'
                        onClick={() => submitCode(sourceCode, sourceLanguageId, setOutput)}>
                    Exécuter
                    </Button>
                    {/*<button className='btn' onClick={() => submitCode(sourceCode, idLanguage, setOutput)}> Exécuter</button>*/}
                </div>

                <div className='output' id='output'>
                    {/* Circular progress centered in parent div */}
                    {isExecuting && <CircularProgress className={'CircularLoader'} />}
                    {output}
                </div>

                <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js' />
                <script src='ui/js/lib/ace.js' />
                <script src='ui/js/lib/theme-monokai.js' />
                <script src='ui/js/ide.js' />
            </body>
        </div>
    );
}

export default Editor;
