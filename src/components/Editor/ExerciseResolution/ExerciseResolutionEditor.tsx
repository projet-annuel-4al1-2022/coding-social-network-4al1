import './ExerciseResolutionEditor.css';
import React, { useState } from 'react';
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-java';
import 'ace-builds/src-noconflict/mode-c_cpp';
import 'ace-builds/src-noconflict/mode-python';
import 'ace-builds/src-noconflict/mode-csharp';
import 'ace-builds/src-noconflict/theme-monokai';
import axios from 'axios';
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Button,
    CircularProgress,
    Grid,
    Typography,
} from '@mui/material';
import {
    codeExecutorAPIUrl,
    findLanguageByName,
    languagesHighlightRules,
} from '../../../services/code-executor.service';
import { ChallengeService } from '../../collaborative/challenges/Challenge.service';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Dialog from '@mui/material/Dialog';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { ColorService } from '../../../services/color.service';

let cloneDefaultLanguage = null;
let cloneDefaultLanguageId = -1;


interface ValidatingTestCaseResult {
    output: string;
    executionTime: number;
    memory: number;
    hasExecutionError: boolean;
}

export function ExerciseResolutionEditor({
    userId,
    challengeId,
    exercise,
    parentCallback,
    defaultCode,
    defaultLanguage,
    setNumberOfValidatedTests,
    setHasExecutedOnce,
}) {
    if (cloneDefaultLanguage == null) {
        if (defaultLanguage != '') {
            cloneDefaultLanguage = defaultLanguage;
            cloneDefaultLanguageId = findLanguageByName(cloneDefaultLanguage) != null ? findLanguageByName(cloneDefaultLanguage).id : -1;
        }
    }

    const [isExecuting, setIsExecuting] = useState(false);
    const [validatingTests] = useState(exercise.validatingTests);
    const [validatedCases, setValidatedCases] = useState([]);

    function changeLanguage(e) {
        const selectedID = e.target.value;
        const foundLanguageById = languagesHighlightRules.find(language => language.id == selectedID);
        cloneDefaultLanguage = foundLanguageById.value;
        cloneDefaultLanguageId = foundLanguageById.id;
    }

    function uploadExecuteAndCheckUserCode(code, idLanguage) {
        if (cloneDefaultLanguage != exercise.language) { // Wrong language
            alert('Veuillez choisir le langage correspondant à l\'exercice (' + exercise.language + ')');
            setIsExecuting(false);
            return;
        }

        setValidatedCases([]);
        setIsExecuting(true);
        handleClickOpen();
        // Upload user code
        ChallengeService.uploadCode(userId, challengeId, code, defaultLanguage).then(() => {
            // For each validatingTests -> check if user's code passes them or not
            const validatingTestsData = [];
            for (let i = 0; i < validatingTests.length; i++) {
                validatingTestsData.push({
                    source_code: `${code}\n\n${validatingTests[i].codeToExecute}`,
                    language_id: idLanguage,
                });
            }

            // Parallelize promise to execute all validating tests
            const promises = validatingTestsData.map(validatingTestData => {
                return executeValidatingTestsWithUserCode(validatingTestData);
            });

            Promise.all(promises).then(responses => {
                const testsResults = [];
                // What we need :
                // status
                // input
                // expectedOutput
                // output

                for (let i = 0; i < responses.length; i++) {
                    const validatingTestCaseResult = responses[i] as ValidatingTestCaseResult;
                    const validatingTestCase = validatingTests[i];
                    console.log('Comparing output and expected : ' + validatingTestCaseResult.output + ' and ' + validatingTestCase.expectedOutput);
                    const validatedTestCaseResult = {
                        input: validatingTestCase.input,
                        expectedOutput: validatingTestCase.expectedOutput,
                        status: validatingTestCaseResult.hasExecutionError ? false : validatingTestCaseResult.output.trim() == validatingTestCase.expectedOutput.trim(),
                        output: validatingTestCaseResult.output,
                        executionTime: validatingTestCaseResult.executionTime,
                        memory: validatingTestCaseResult.memory,
                        hasExecutionError: validatingTestCaseResult.hasExecutionError,
                    };
                    testsResults.push(validatedTestCaseResult);
                }
                const averageExecutionTime = responses.reduce((acc, curr) => acc + curr.executionTime, 0) / responses.length;
                const averageUsedMemory = responses.reduce((acc, curr) => acc + curr.memory, 0) / responses.length;
                ChallengeService.updateUserCodeExecutingTimeAndMemory(userId, challengeId, averageExecutionTime, averageUsedMemory);

                setNumberOfValidatedTests(testsResults.filter(testResult => testResult.status).length);

                setValidatedCases(testsResults);
                setIsExecuting(false);
                setHasExecutedOnce(true);
            });
        });
    }

    async function executeValidatingTestsWithUserCode(codeToExecuteWithLanguage: {
        source_code: string,
        language_id: number,
    }): Promise<ValidatingTestCaseResult> {
        const config = {
            method: 'post',
            url: codeExecutorAPIUrl + '/submissions',
            headers: {
                'Content-Type': 'application/json',
            },
            data: codeToExecuteWithLanguage,
        };

        try {
            const response = await axios(config);
            console.log('First judge0 response', response);
            let token = JSON.stringify(response.data.token);
            token = token.split('"')[1];
            // wait for 3 seconds before getting output code
            await new Promise(resolve => setTimeout(resolve, 3000));
            return getOutputCode(token);
            // setTimeout(() => {
            //     return getOutputCode(token);
            // }, 3000, token, setOutput);
        } catch (error) {
            console.log('Error when executing code : ', codeToExecuteWithLanguage.source_code, '\nGot error : ', error);
            return {
                output: '',
                executionTime: 0,
                memory: 0,
                hasExecutionError: true,
            };
        }
    }

    async function getOutputCode(token): Promise<ValidatingTestCaseResult> {
        const apiURL = codeExecutorAPIUrl + '/submissions/' + token;

        const config = {
            method: 'get',
            url: apiURL,
            headers: {},
        };

        return executeCodeAndUpdateChallengeInfos(config);
    }

    async function executeCodeAndUpdateChallengeInfos(config): Promise<ValidatingTestCaseResult> {
        try {
            const response = await axios(config);
            console.log('Second judge0 response', response);

            const stdout = JSON.stringify(response.data.stdout);
            const executionTime = JSON.parse(response.data.time);
            const memory = JSON.parse(response.data.memory);
            let hasExecutionError = false;

            let result;
            if (stdout === 'null') {
                result = JSON.stringify(response.data.stderr);
                hasExecutionError = true;
            } else {
                result = stdout;
            }
            result = JSON.parse(result);
            return {
                output: result,
                executionTime,
                memory,
                hasExecutionError,
            };
        } catch (error) {
            console.log('Error when getting output code, got error : ', error);
            return {
                output: '',
                executionTime: 0,
                memory: 0,
                hasExecutionError: true,
            };
        }

    }

    const [dialogOpen, setDialogOpen] = useState(false);

    const handleClickOpen = () => {
        setDialogOpen(true);
    };

    const handleClose = () => {
        setDialogOpen(false);
    };

    return (
        <div className='App'>
            <head>
                <title>CSN Online IDE</title>
            </head>
            {!cloneDefaultLanguage || !cloneDefaultLanguageId && (
                <>
                    <CircularProgress className={'CircularLoader'} />
                </>
            )}
            {cloneDefaultLanguage && cloneDefaultLanguageId && (
                <>
                    <div>
                        <div className='header EditorHeader'>Compilateur en ligne</div>
                        <div className='control-panel'>
                            Sélectionner le langage
                            &nbsp; &nbsp;
                            <select id='languages' className='languages'
                                defaultValue={cloneDefaultLanguageId}
                                onChange={(e) => changeLanguage(e)}
                            >
                                {languagesHighlightRules.map((language) => (
                                    <option key={language.id} value={language.id}>{language.label}</option>
                                ))}
                            </select>
                        </div>

                        <div className='editor' id='editor'>
                            <AceEditor
                                height='100%'
                                width='100%'
                                mode={defaultLanguage}
                                theme='monokai'
                                name='aceEditor'
                                onChange={(codeContent) => {
                                    defaultCode = codeContent;
                                    if (parentCallback) {
                                        parentCallback(codeContent, defaultLanguage);
                                    }
                                }}
                                value={defaultCode}
                                fontSize={14}
                                showPrintMargin={true}
                                showGutter={true}
                                highlightActiveLine={true}
                                setOptions={{
                                    enableBasicAutocompletion: true,
                                    enableLiveAutocompletion: true,
                                    enableSnippets: false,
                                    showLineNumbers: true,
                                    tabSize: 4,
                                    readOnly: false,
                                }}
                            />
                        </div>

                        <div className='button-container'>
                            <Button variant='contained' className='CapitalizedButton' color='success'
                                onClick={() => uploadExecuteAndCheckUserCode(defaultCode, cloneDefaultLanguageId)}>
                                Exécuter
                            </Button>
                        </div>
                    </div>

                    {/* Execution code & validating tests dialog */}
                    <Dialog open={dialogOpen} onClose={handleClose} fullWidth={true} maxWidth={'lg'}>
                        <DialogTitle>Exécution et validation de l'exercice</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                Vous pouvez voir la sortie console de votre code et les tests que vous avez validé sur
                                cette exercice.
                            </DialogContentText>

                            {isExecuting && (
                                <CircularProgress style={{
                                    position: 'absolute',
                                    top: '50%',
                                    left: '50%',
                                }} />
                            )}

                            {/*<div className='output' id='output'>*/}
                            {/*    {output}*/}
                            {/*</div>*/}

                            {/* Validating tests */}
                            <div className='ValidatingTests'>
                                <div className='validating-tests-header'>
                                    <Typography variant='h6'>
                                        Tests validés :
                                    </Typography>
                                    <div className='validating-tests-header-info'>
                                        {validatedCases.length} / {validatingTests.length}
                                    </div>
                                </div>
                                <div className='validating-tests-body'>
                                    {validatedCases.map((validatingCase, index) => (
                                        <Accordion key={index} sx={{
                                            backgroundColor: ColorService.APP_PRIMARY_COLOR,
                                            color: ColorService.INVERTED_APP_PRIMARY_COLOR,
                                        }}>
                                            <AccordionSummary
                                                expandIcon={<ExpandMoreIcon />}
                                                aria-controls='panel1a-content'
                                                id='panel1a-header'
                                            >
                                                <Grid container spacing={1}>
                                                    <Grid item>
                                                        <Typography variant='h6'>
                                                            Test {index + 1} : {validatingCase.name}
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography variant='h6'>
                                                            {/* If status is true -> display ✅ */}
                                                            {/* If status is false -> display ❌ */}
                                                            {validatingCase.status ? (
                                                                <CheckCircleIcon className={'ValidatingCaseStatusIcon'}
                                                                    color='success' />
                                                            ) : (
                                                                <CancelIcon className={'ValidatingCaseStatusIcon'}
                                                                    color='error' />
                                                            )}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>


                                            </AccordionSummary>
                                            <AccordionDetails>
                                                <div className='validating-test-body'>
                                                    <div className='validating-test-body-title'>
                                                        Paramètres en entrée :
                                                    </div>
                                                    <div className='validating-test-body-input'>
                                                        {validatingCase.input}
                                                    </div>
                                                    <div className='validating-test-body-title'>
                                                        Sortie attendue
                                                    </div>
                                                    <div className='validating-test-body-output'>
                                                        {validatingCase.expectedOutput}
                                                    </div>
                                                    <div className='validating-test-body-title'>
                                                        Sortie obtenue
                                                    </div>
                                                    <div className='validating-test-body-output'>
                                                        {validatingCase.output}
                                                    </div>
                                                </div>
                                            </AccordionDetails>
                                        </Accordion>
                                    ))}
                                </div>
                            </div>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose}>Fermer</Button>
                        </DialogActions>
                    </Dialog>
                </>
            )}
        </div>
    );
}
