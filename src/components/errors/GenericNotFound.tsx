import React from 'react';
import { Link } from 'react-router-dom';
import notFound from '../../assets/img/errors/not_found.png';

export function GenericNotFound() {
    return (
        <div className="Centered">
            <img src={notFound} alt="404 - Not Found!" className="NotFound" />
            <br />
            <br />
            <Link to="/" className="Bold GoHome">
                Go Home
            </Link>
        </div>
    );
}
