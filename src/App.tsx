import React from 'react';
import './App.css';
import { BrowserRouter as Router, Navigate, Route, Routes } from 'react-router-dom';
import { SignUp } from './components/authentification/SignUp';
import { Dashboard } from './components/dashboard/Dashboard';
import { Login } from './components/authentification/Login';
import useToken from './components/authentification/useToken';
import { GenericNotFound } from './components/errors/GenericNotFound';
import { Snippets } from './components/snippets/Snippets';
import { PrimarySearchAppBar } from './components/appbar/Appbar';
import PostsList from './components/userPages/postsCard/PostsList';
import SkillsList from './components/userPages/skillsCard/SkillsList';
import UserNavBar from './components/appbar/UserNavBar';
import { UserPrivateProfileCard } from './components/userPages/userCard/UserPrivateProfileCard';
import Editor from './components/Editor/Editor';
import { PostDetails } from './components/dashboard/posts/details/PostDetails';
import { UserPublicProfile } from './components/userPages/userCard/UserPublicProfile';
import { Exercises } from './components/collaborative/challenges/Exercises';
import { ExerciseDetails } from './components/collaborative/challenges/exercise-details/ExerciseDetails';
import { ResolveExercise } from './components/collaborative/challenges/resolve-exercise/ResolveExercise';
import { UserOnGoingChallenges } from './components/collaborative/challenges/user-challenges/UserOnGoingChallenges';
import {
    UserChallengeDetails,
} from './components/collaborative/challenges/user-challenges/details/UserChallengeDetails';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import MesChallenges from './components/mesChallenges/MesChallenges';

function App() {
    const { accessToken, setToken } = useToken();

    const theme = createTheme({
        palette: {
            primary: {
                main: '#E5B248',
                contrastText: '#fff',
            },
            secondary: {
                main: '#282733',
                contrastText: '#fff',
            },
            success: {
                main: '#448805',
                contrastText: '#fff',
            },
        },
    });

    if (!accessToken) {
        return (
            <Router>
                <ThemeProvider theme={theme}>
                    <Routes>
                        <Route path='/' element={<Login setToken={setToken} />} />
                        <Route
                            path='/login'
                            element={<Login setToken={setToken} />}
                        />
                        <Route path='/signup' element={<SignUp />} />
                        <Route path='*' element={<GenericNotFound />} />
                    </Routes>
                </ThemeProvider>
            </Router>
        );
    }

    return (
        <Router>
            <ThemeProvider theme={theme}>
                <PrimarySearchAppBar />
                <Routes>
                    <Route
                        path='/login'
                        element={
                            accessToken ? (
                                <Navigate replace to={'/dashboard'} />
                            ) : (
                                <Login setToken={setToken} />
                            )
                        }
                    />
                    <Route
                        path='/signup'
                        element={
                            accessToken ? (
                                <Navigate replace to={'/dashboard'} />
                            ) : (
                                <SignUp />
                            )
                        }
                    />
                    <Route path='/' element={<Dashboard />} />
                    <Route path='/dashboard' element={<Dashboard />} />
                    <Route path='/snippets' element={<Snippets />} />
                    <Route
                        path='/user/profile'
                        element={
                            <>
                                <UserNavBar />
                                <UserPrivateProfileCard />
                            </>
                        }
                    />
                    <Route
                        path='/user/:userId/profile'
                        element={
                            <>
                                <UserPublicProfile />
                            </>
                        }
                    />
                    <Route
                        path='/user/posts'
                        element={
                            <>
                                <UserNavBar />
                                <PostsList />
                            </>
                        }
                    />
                    <Route
                        path='/user/skills'
                        element={
                            <>
                                <UserNavBar />
                                <SkillsList />
                            </>
                        }
                    />
                    <Route
                        path='/post/:postId/details'
                        element={
                            <>
                                <PostDetails />
                            </>
                        }
                    />
                    {/* User Challenges */}
                    <Route
                        path='/challenges/on-going'
                        element={
                            <>
                                <UserOnGoingChallenges />
                            </>
                        }
                    />
                    <Route
                        path='/challenges/:challengeId/details'
                        element={
                            <>
                                <UserChallengeDetails />
                            </>
                        }
                    />
                    {/* Exercises */}
                    <Route
                        path='/exercises'
                        element={
                            <>
                                <Exercises />
                            </>
                        }
                    />
                    <Route
                        path='/exercises/:exerciseId/details'
                        element={
                            <>
                                <ExerciseDetails />
                            </>
                        }
                    />
                    <Route
                        path='/challenges/:challengeId/resolution'
                        element={
                            <>
                                <ResolveExercise />
                            </>
                        }
                    />

                    <Route path='*' element={<GenericNotFound />} />

                    <Route
                        path='/Editor'
                        element={
                            <>
                                <Editor isSandbox={true} isUsedInForm={false} ref={undefined} />
                            </>
                        }
                    />

                    <Route
                        path='/meschallenges'
                        element={
                            <MesChallenges />
                        }
                    />
                </Routes>
            </ThemeProvider>
        </Router>
    );
}

export default App;
