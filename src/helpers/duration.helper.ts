import * as momentTz from 'moment-timezone';

export class Duration {
    static timeSinceNow(date: string, tz: string, locale: string): string {
        return momentTz.utc(date).tz(tz).lang(locale).locale(locale).fromNow();
    }

    static timeSinceFrench(date: string): string {
        return `${this.timeSinceNow(date, 'Europe/Paris', 'fr')}`;
    }
}
