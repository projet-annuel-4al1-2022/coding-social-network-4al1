export const codeExecutorAPIUrl = 'https://judge0-ce.p.rapidapi.com/';

export const languagesHighlightRules = [
    {
        label: '',
        value: '',
        id: -1,
    },
    {
        label: 'C (GCC 9.2.0)',
        value: 'c',
        id: 50,
    },
    {
        label: 'C++ (GCC 9.2.0)',
        value: 'c_cpp',
        id: 54,
    },
    {
        label: 'C#',
        value: 'csharp',
        id: 51,
    },
    {
        label: 'Java (JDK 13)',
        value: 'java',
        id: 62,
    },
    {
        label: 'Python (3.8.1)',
        value: 'python',
        id: 71,
    },
];

export const findLanguageByName = (languageName) => {
    return languagesHighlightRules.find((language) => {
        return language.value == languageName;
    });
};