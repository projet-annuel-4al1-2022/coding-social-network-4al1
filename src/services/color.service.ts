export class ColorService {
    static readonly APP_PRIMARY_COLOR = '#282733';
    static readonly APP_SECONDARY_COLOR = '#E5B248';
    static readonly APP_SECONDARY_HOVER_COLOR = '#dc9724';
    static readonly INVERTED_APP_PRIMARY_COLOR = '#FFFFFF';
    // static readonly APP_PRIMARY_COLOR = '#3974CC';
}
