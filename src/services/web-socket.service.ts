import { io } from 'socket.io-client';
import UserService from './user.service';

export const socket = io('http://localhost:3000', {
    extraHeaders: {
        Authorization: `Bearer ${UserService.getAccessToken() || ''}`,
    },
});