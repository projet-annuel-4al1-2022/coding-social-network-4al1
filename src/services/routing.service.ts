import UserService from './user.service';

export function goToSignUp() {
    window.location.href = '/signup';
}

export function goToLogin() {
    window.location.href = '/login';
}

export function goToDashboard() {
    window.location.href = '/dashboard';
}

export function goToSnippets() {
    window.location.href = '/snippets';
}

export function logout() {
    UserService.logout();
    goToLogin();
}

export function goToUserPages() {
    window.location.href = '/user/posts';
}

export function goToUserProfile() {
    window.location.href = '/user/profile';
}

export function goToOtherUserProfile(userId: number) {
    window.location.href = '/user/' + userId + '/profile';
}

export function goToUserPosts() {
    window.location.href = '/user/posts';
}

export function goToUserSkills() {
    window.location.href = '/user/skills';
}

export function goToPostDetails(postId) {
    window.open(`/post/${postId}/details`, '_blank');
}

export function goToPostDetailsFromSearch(postId) {
    window.location.href = `/post/${postId}/details`;
}

export function goToExercises() {
    window.location.href = '/exercises';
}

export function goToUserOnGoingChallenges() {
    window.location.href = '/challenges/on-going';
}

export function goToChallengeDetails(challengeId: number | string) {
    window.location.href = '/challenges/' + challengeId + '/details';
}

export function goToExerciseDetails(challengeId) {
    window.location.href = '/exercises/' + challengeId + '/details';
}

export function goToExerciseResolution(exerciseId) {
    window.location.href = '/challenges/' + exerciseId + '/resolution';
}

export function goToMyChallengesHistory(){
    window.location.href = '/meschallenges';
}