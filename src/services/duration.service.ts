export class DurationService {
    static readonly HOUR_SYMBOL = 'h ';
    static readonly MINUTE_SYMBOL = 'm ';
    static readonly SECOND_SYMBOL = 's ';

    static formatChronoTime(time: string) {
        return time.length <= 1 ? `0${time}` : time;
    }

    static getFormattedSecondsInHourMinuteSeconds(numberOfSeconds: number) {
        const hours = Math.floor(numberOfSeconds / 3600);
        const minutes = Math.floor((numberOfSeconds - hours * 3600) / 60);
        const seconds = numberOfSeconds - hours * 3600 - minutes * 60;
        const minuteString = DurationService.formatChronoTime(minutes.toString());
        const hourString = DurationService.formatChronoTime(hours.toString());
        const secondsString = DurationService.formatChronoTime(seconds.toString());
        return hourString + DurationService.HOUR_SYMBOL + minuteString + DurationService.MINUTE_SYMBOL + secondsString + DurationService.SECOND_SYMBOL;
    }
}