export default class ChallengeNotificationService {
    static addUserChallengeNotification(notification, userId) {
        // Get all notifications
        const notifications = this.getUserChallengeNotifications(userId);
        // Add new notification
        notifications.push(notification);
        // Save all notifications
        sessionStorage.setItem('notifications-' + userId, JSON.stringify(notifications));
    }

    static addUserChallengeNotifications(notifications, userId) {
        // Get all notifications
        const userNotifications = JSON.parse(
            sessionStorage.getItem('notifications-' + userId) ?? '[]',
        );
        // Add new notifications
        userNotifications.push(...notifications);
        // Save all notifications
        sessionStorage.setItem('notifications-' + userId, JSON.stringify(userNotifications));
    }

    static removeUserChallengeNotification(notification, userId) {
        // Get all notifications
        const notifications = this.getUserChallengeNotifications(userId);
        // Remove notification
        notifications.splice(notifications.indexOf(notification), 1);
        // Save all notifications
        sessionStorage.setItem('notifications-' + userId, JSON.stringify(notifications));
    }

    static getUserChallengeNotifications(userId) {
        return JSON.parse(
            sessionStorage.getItem('notifications-' + userId) ?? '[]',
        );
    }
}
