import { goToLogin } from './routing.service';

export default class UserService {
    static getCurrentUser() {
        const currentUser = JSON.parse(sessionStorage.getItem('user'));
        if (!currentUser) {
            goToLogin();
        }
        return currentUser;
    }

    static setCurrentUser(user) {
        sessionStorage.setItem('user', JSON.stringify(user));
    }

    static getAccessToken() {
        return JSON.parse(sessionStorage.getItem('accessToken')) ?? null;
    }

    static setAccessToken(accessToken) {
        sessionStorage.setItem('accessToken', JSON.stringify(accessToken));
    }

    static logout() {
        sessionStorage.removeItem('accessToken');
        sessionStorage.removeItem('user');
    }
}
