export class ApiService {
    static async get<T>(url: string, options?: RequestInit): Promise<T> {
        const response = await fetch(url, options);
        if (200 < response['status'] && response['status'] > 299) {
            throw new Error('Failed calling API with url: ' + url);
        }
        return response.json();
    }

    static async post<T>(
        url: string,
        body?: any,
        options?: RequestInit,
    ): Promise<T> {
        const response = await fetch(url, {
            ...options,
            headers: { 'Content-Type': 'application/json' },
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(body),
        });

        if (200 < response['status'] && response['status'] > 299) {
            throw new Error('Failed calling API with url: ' + url);
        }
        return response.json();
    }

    static async patch<T>(
        url: string,
        body?: any,
        options?: RequestInit,
    ): Promise<T> {
        return (await fetch(url, {
            ...options,
            headers: { 'Content-Type': 'application/json' },
            method: 'PATCH',
            mode: 'cors',
            body: JSON.stringify(body),
        })).json();
    }

    static async delete<T>(url: string, options?: RequestInit): Promise<T> {
        return (await fetch(url, {
            ...options,
            headers: { 'Content-Type': 'application/json' },
            method: 'DELETE',
            mode: 'cors',
        })).json();
    }
}
